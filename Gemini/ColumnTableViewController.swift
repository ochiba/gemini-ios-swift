import UIKit

class ColumnTableViewController : GeminiTableViewController {

    var chart               = false;
    public var display      = ["", "", "", "", ""];
    var arrayColumns        = [String]();
    var arraySelected3Columns = [Int]();
    public var tableName    = "";
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayColumns.count;
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    func display(_ index: Int) -> String {
        return display[index];
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        
        self.title = ((iPad && !chart) ? "Columns (Select up to 5)" : "Columns (Select up to 3)");
       
        
        // Default one (filter in this case)
        if (!iPad || chart) {
            self.navigationItem.rightBarButtonItem?.customView?.alpha = 1.0;
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "columnCell", for: indexPath)
        
        // For non-information sections, arrayRaces must be non-zero.
        if (arrayColumns.count > 0) {
            cell.textLabel?.text = arrayColumns[indexPath.row];
            cell.detailTextLabel?.text   = "";
            
            for j in 0..<arraySelected3Columns.count {
                
                if (arraySelected3Columns[j] == indexPath.row)
                {
                    cell.detailTextLabel?.text = "\(j + 1)";
                    break;
                }
            }
        }
        return cell;
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        startDownloading();
    }

    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated);
        returnDisplayColumns();
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var bRemoved = false;
        let limit = (iPad && !chart) ? 5 : 3;
            
        for i in 0..<arraySelected3Columns.count
        {
            if (arraySelected3Columns[i] == indexPath.row)
            {
                arraySelected3Columns.remove(at: i);
                bRemoved = true;
                break;
            }
        }

        if (!bRemoved && arraySelected3Columns.count < limit)
        {
            arraySelected3Columns.append(indexPath.row);
        }
        
        self.tableView.reloadData();
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        returnDisplayColumns();
    }

    override func startDownloading()
    {
        super.startDownloading();

        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=10&table=\(self.tableName)&time_only=0")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                self.arrayColumns = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String];
                
                if (self.arrayColumns.isEmpty) {
                    return;
                }
                
                self.arraySelected3Columns.removeAll();
                
                for k in stride(from: (self.arrayColumns.count - 1), through: 0, by: -1) {
                    
                    let oneColumn = self.arrayColumns[k];
                    
                    if (oneColumn.caseInsensitiveCompare("First Name") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Last Name") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Overall") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Bib") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("ID") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("State") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Country") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Total Runners") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Gender Total") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("Division Total") == ComparisonResult.orderedSame ||
                        oneColumn.caseInsensitiveCompare("TOTAL COMPETITORS") == ComparisonResult.orderedSame)
                    {
                        self.arrayColumns.remove(at: k);
                    }
                }
                
                if (self.iPad)
                {
                    for j in 0...4 {
                        
                        if ((j < 3) || (j >= 3 && !self.chart)) {
                            // Add display fields, one at a time.
                            if (self.display[j] != "")
                            {
                                for i in 0..<self.arrayColumns.count
                                {
                                    // Select the current selections in the Search View:
                                    if (self.arrayColumns[i] == self.display[j])
                                    {
                                        self.arraySelected3Columns.append(i);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                self.dataDownloaded();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }
    
    
    // Called in the Search View.  Current selections in the view is specified here.
    func returnDisplayColumns()
    {
        for j in 0...4 {
            display[j] = "";
        }
        
        for i in 0..<arraySelected3Columns.count
        {
            display[i] = arrayColumns[arraySelected3Columns[i]];
        }
    }

    // Called in the Search View.  Current selections in the view is specified here.
    func setDisplayColumns(_ newDisplay: [String])
    {
        for i in 0...4 {
            display[i] = newDisplay[i];
        }
    }
}

//
//- (void) setChart: (BOOL) newChart keyword: (NSString*) newKeyword division: (BOOL) newDivision top3: (BOOL) newTop3
//{
//    chart           = newChart;
//    bTop3           = newTop3;
//    bDivision       = newDivision;
//    currentKeyword  = newKeyword;
//}
