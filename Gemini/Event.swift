//
//  Event.swift
//  
//
//  Created by Osamu Chiba on 6/15/17.
//
//

import UIKit

class Event: NSObject {
    var post_title: String      = "";
    var post_date: String       = "";
    var ID: Int                 = 0;
    var city: String            = "";
    var state: String           = "";
    var post_status: String     = "";
    var post_modified: String   = "";
    
    init(_ pk: Int)
    {
        ID = pk;
        super.init();
    }
}
