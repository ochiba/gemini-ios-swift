import UIKit
//#ifdef YOUTUBE
//#import "YTPlayerViewController.h"
//#endif
let FAVORITE_SECTION        = 0
let EVENT_SECTION           = 1
let ATHLETE_SECTION         = 2;
let OFFICIAL_TIME_SECTION   = 3;
let RANKING_SECTION         = 4;
let DETAILS_SECTION         = 5;

// #define YOUTUBE_SECTION 0
class OneResultTableViewController : GeminiTableViewController {

    var dataArray       = [String]();
    var columnArray     = [String]();
    var removeFollow    = false;
    var bAddToFavorites = false;
    var quickResult: QuickResult?;
    var detailResult: DetailResult?;

    weak var chartSelectionTableViewController: ChartSelectionTableViewController?;

    override func viewDidLoad()
    {
        super.viewDidLoad();
    
        // If the parent is Favorite, then the choice must be remove from the list.  Otherwise, add to Favorites.
        bAddToFavorites = TAB_BOOKMARKS != self.tabBarController?.selectedIndex;
        
        self.startDownloading();
       
    //#ifdef YOUTUBE
    //    // Add another button if this event is not a future event.
    //    // The extra button is used to display different column.
    //    if (![self isFuture])
    //    {
    //        // Get the existing right button array, which already has Filter button.
    //        NSArray* btnArray = self.navigationItem.rightBarButtonItems;
    //        NSMutableArray *arrRightBarItems = [[NSMutableArray alloc] initWithArray: btnArray];
    //        
    //        
    //        UIBarButtonItem *pixButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCamera target: self action: @selector(showPix:)];
    //        
    //        [arrRightBarItems addObject: pixButton];
    //        
    //        // Reset the right button array.
    //        self.navigationItem.rightBarButtonItems = arrRightBarItems;
    //    }
    //#endif
        // No social media needed for future events.
        self.navigationItem.rightBarButtonItem?.isEnabled = !(self.future());
    }

    override func startDownloading()
    {
        super.startDownloading();
        
        let escapedLastName = quickResult!.lastName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedFirstName = quickResult!.firstName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);

        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=8&table=\(quickResult!.tableName)&first_name=\(escapedFirstName!)&last_name=\(escapedLastName!)&bib_number=\(quickResult!.bibNumber)");
        
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                let newData = self.generateDetailResult(column: json[0] as! [String], data: json[1] as! [String], quickResult: self.quickResult!);

                self.columnArray    = newData.columnArray;
                self.dataArray      = newData.dataArray;
                self.detailResult   = newData.detailResult;
                
                self.dataDownloaded();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return DETAILS_SECTION + 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if (section == OFFICIAL_TIME_SECTION) {
            return 1;
        }

        //#ifdef YOUTUBE
        //    if (section == YOUTUBE_SECTION)
        //        return 1;
        //#endif
        if (iPad)
        {
            if (section <= EVENT_SECTION) {
                return 1;
            }
            else if (section == ATHLETE_SECTION) {
                return 4;
            }
            else if (section == RANKING_SECTION) {
                return 2;
            }
            else if (section == DETAILS_SECTION)
            {
                var i = 0;
                
                if (columnArray.count % 3 != 0) {
                    i += 1;
                }
                
                // 2 means the number of rows are doubled because of title / data combinations.
                return (columnArray.count / 3 + i) * 2;
            }
        }
        
        // iPhone
        switch (section) {
            case EVENT_SECTION:
                return 4;
            
            case ATHLETE_SECTION:
                return 6;
            
            case RANKING_SECTION:
                return 3;
            
            case DETAILS_SECTION:
                return columnArray.count;
            
            default:
                return 1;
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String
    {
        //#ifdef YOUTUBE
        //    if (section == YOUTUBE_SECTION)
        //        return "";
        //#endif
    
        if (iPad && (section <= EVENT_SECTION)) {
            return "";
        }
        else if (!iPad && (section == EVENT_SECTION)) {
            return "Event";
        }
        else if (section == ATHLETE_SECTION) {
            return "Athlete";
        }
        else if (section == OFFICIAL_TIME_SECTION) {
            return "Official Time";
        }
        else if (section == RANKING_SECTION) {
            return "Rankings";
        }
        else if (section == DETAILS_SECTION) {
            
            if (!iPad && self.columnArray.isEmpty) {
                return "";
            }
            
            return "Details";
        }

        return "";
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (iPad && indexPath.section == EVENT_SECTION) {
            return 101;
        }
        
        return 44;
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view : UIView, forSection section: Int) {
        if (!iPad) {
            return;
        }

        let header = view as! UITableViewHeaderFooterView;
        
        header.contentView.backgroundColor  = geminiRed;
        header.textLabel?.textColor         = UIColor.white;
        header.textLabel?.font              = UIFont.boldSystemFont(ofSize: 17);
        header.textLabel?.frame             = header.frame;
        header.textLabel?.textAlignment     = .center;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //#ifdef YOUTUBE
        //    if (indexPath.section == YOUTUBE_SECTION) {
        //        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: "youTubeCell"];
        //        cell.textLabel?.text = "Video Clip";
        //        return cell;
        //    }
        //#endif
        
        if (indexPath.section == FAVORITE_SECTION)
        {
            if (bAddToFavorites)
            {
                if (self.future()) {
                    return tableView.dequeueReusableCell(withIdentifier: "followCell")!;
                }
                else {
                    return tableView.dequeueReusableCell(withIdentifier: "addFavoriteCell")!;
                }
            }
            else {
                return tableView.dequeueReusableCell(withIdentifier: "removeFavoriteCell")!;
            }
        }
        else if (indexPath.section == OFFICIAL_TIME_SECTION && self.detailResult != nil)
        {
            let timeCell = tableView.dequeueReusableCell(withIdentifier: "officialTimeCell", for: indexPath)  as! OfficialTimeTableViewCell;
            
            timeCell.labelOfficialTime?.text = self.detailResult!.chipTime.replacingOccurrences(of: "1", with: " 1");
            // timeCell.textLabel?.font = UIFont(name: "DBLCDTempBlack", size: 45)!;
            
            return timeCell;
        }
        
        if (iPad)
        {
            var cellName = ((indexPath.row % 2) == 0) ? "headerCell" : "dataCell";
            
            if (indexPath.section == EVENT_SECTION) {
                cellName = "eventCell";
            }
          
            let cell = tableView.dequeueReusableCell(withIdentifier: cellName) as! ThreeColumnTableViewCell;
            
            if (indexPath.section == EVENT_SECTION)
            {
                cell.contentView.backgroundColor = UIColor(red: 0.333333343, green: 0.333333343, blue: 0.333333343, alpha: 1.0);
                
                cell.labelDisplay1?.text    = currentEvent!.post_title.replacingOccurrences(of: "\\'", with: "'");
                cell.labelDisplay2?.text    = self.formatDate(currentEvent!.post_date) + "   \(currentEvent!.city), \(currentEvent!.state)";
                cell.labelDisplay3?.text    = self.quickResult!.raceTitle;
            }
            else if (indexPath.section == ATHLETE_SECTION)
            {
                if (indexPath.row == 0)
                {
                    cell.labelDisplay1?.text = "Name";
                    cell.labelDisplay2?.text = "Age";
                    cell.labelDisplay3?.text = "Gender";
                }
                else if (indexPath.row == 1 && self.detailResult != nil)
                {
                    cell.labelDisplay1?.text = (self.detailResult!.firstName == "" && self.detailResult!.lastName == "" ) ? "Unknown" : "\(self.detailResult!.firstName) \(self.detailResult!.lastName)";
                    cell.labelDisplay2?.text = (self.detailResult!.age == 0) ? "N/A" : "\(self.detailResult!.age)";
                    cell.labelDisplay3?.text = (self.detailResult!.gender == "") ? "N/A" : self.detailResult!.gender;
                }
                else if (indexPath.row == 2)
                {
                    cell.labelDisplay1?.text = "Hometown";
                    cell.labelDisplay2?.text = "Bib Number";
                    cell.labelDisplay3?.text = "Division";
                }
                else if (indexPath.row == 3 && self.detailResult != nil)
                {
                    var hometown = self.detailResult!.city;
                    
                    if (hometown != "" && self.detailResult!.state != "") {
                        hometown += ", " + self.detailResult!.state;
                    }
                    else if (hometown == "") {
                        hometown = self.detailResult!.state;
                    }
                    
                    if (hometown != "" && self.detailResult!.country != "") {
                        hometown += " " + self.detailResult!.country;
                    }
                    
                    if (hometown == "") {
                        hometown = "N/A";
                    }

                    cell.labelDisplay1?.text = hometown;
                    cell.labelDisplay2?.text = (self.detailResult!.bibNumber == 0) ? "N/A" : "#\(self.detailResult!.bibNumber)";
                    cell.labelDisplay3?.text = (self.detailResult!.division == "") ? "N/A" : self.detailResult!.division;
                }
            }
            else if (indexPath.section == RANKING_SECTION && self.detailResult != nil)
            {
                if (indexPath.row == 0)
                {
                    cell.labelDisplay1?.text = "Overall";
                    cell.labelDisplay2?.text = "Division";
                    cell.labelDisplay3?.text = "Gender";
                }
                else if (indexPath.row == 1)
                {
                    cell.labelDisplay1?.text = formatRanking(self.detailResult!.overall, self.detailResult!.overallTotal);
                    cell.labelDisplay2?.text = formatRanking(self.detailResult!.divisionPlace, self.detailResult!.divisionTotal);
                    cell.labelDisplay3?.text = formatRanking(self.detailResult!.genderPlace, self.detailResult!.genderTotal);
                }
            }
            else if (DETAILS_SECTION == indexPath.section && dataArray.count > 0)
            {
                let bHeader = (indexPath.row % 2) == 0;
            
                var start = indexPath.row;
                
                if (!bHeader) {
                    start -= 1;
                }
                
                start = (start / 2) * 3;
                
                if (!bHeader)
                {
                    cell.labelDisplay1?.text = dataArray[start].replacingOccurrences(of: "\\'", with: "'");
                    
                    if (dataArray.count > (start + 1)) {
                        cell.labelDisplay2?.text = dataArray[start + 1].replacingOccurrences(of: "\\'", with: "'");
                    }
                    
                    if (dataArray.count > (start + 2))
                    {
                        cell.labelDisplay3?.text = dataArray[start + 2].replacingOccurrences(of: "\\'", with: "'");
                    }
                }
                else
                {
                    cell.labelDisplay1?.text = columnArray[start].replacingOccurrences(of: "\\'", with: "'");

                    if (dataArray.count > (start + 1)) {
                            cell.labelDisplay2?.text = columnArray[start + 1].replacingOccurrences(of: "\\'", with: "'");
                    }

                    if (dataArray.count > (start + 2)) {
                        cell.labelDisplay3?.text = columnArray[start + 2].replacingOccurrences(of: "\\'", with: "'");
                    }
                }
            }
            return cell;
        }
        else // iPhone
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "rankingCell");
        
            if (indexPath.section == EVENT_SECTION)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "dataCell");

                switch (indexPath.row) {
                    case 0:
                        cell?.textLabel?.text = currentEvent!.post_title.replacingOccurrences(of: "\\'", with: "'");
                        break;
                        
                    case 1:
                        cell?.textLabel?.text = self.formatDate((currentEvent!.post_date));
                        break;
                    
                    case 2:
                        cell?.textLabel?.text = "\(currentEvent!.city), \(currentEvent!.state)";
                        break;
                    
                    case 3:
                        cell?.textLabel?.text = self.quickResult!.raceTitle;
                        break;
                    
                    default:
                        break;
                }
                
                return cell!;
            }
            else if (indexPath.section == ATHLETE_SECTION)
            {
                if (indexPath.row == 0)
                {
                    cell?.textLabel?.text = "Name";

                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "Unknown";
                    }
                    else {
                        
                        cell?.detailTextLabel?.text = (self.detailResult!.firstName == "" && self.detailResult!.lastName == "" ) ? "Unknown" : "\(self.detailResult!.firstName) \(self.detailResult!.lastName)";
                    }
                }
                else if (indexPath.row == 1)
                {
                    cell?.textLabel?.text = "Age";

                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "N/A";
                    }
                    else {
                        
                        cell?.detailTextLabel?.text = (self.detailResult!.age == 0) ? "N/A" : "\(self.detailResult!.age)";
                    }
                }
                else if (indexPath.row == 2)
                {
                    cell?.textLabel?.text = "Gender";
                    
                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "N/A";
                    }
                    else {
                        cell?.detailTextLabel?.text = (self.detailResult!.gender == "") ? "N/A" : self.detailResult!.gender;
                    }
                }
                else if (indexPath.row == 3)
                {
                    cell?.textLabel?.text = "Hometown";
                    
                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "N/A";
                    }
                    else {
                        
                        var hometown = self.detailResult!.city;
                        
                        if (hometown != "" && self.detailResult!.state != "") {
                            hometown += ", " + self.detailResult!.state;
                        }
                        else if (hometown == "") {
                            hometown = self.detailResult!.state;
                        }
 
                        if (hometown != "" && self.detailResult!.country != "") {
                            hometown += " " + self.detailResult!.country;
                        }
                        
                        if (hometown == "") {
                            hometown = "N/A";
                        }

                        cell?.detailTextLabel?.text = hometown;
                    }
                }
                else if (indexPath.row == 4)
                {
                    cell?.textLabel?.text = "Bib Number";
                    
                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "N/A";
                    }
                    else {
                        cell?.detailTextLabel?.text = (self.detailResult!.bibNumber == 0) ? "N/A" : "#\(self.detailResult!.bibNumber)";
                    }
                }
                else if (indexPath.row == 5)
                {
                    cell?.textLabel?.text = "Division";
                    
                    if (self.detailResult == nil) {
                        cell?.detailTextLabel?.text = "N/A";
                    }
                    else {
                        cell?.detailTextLabel?.text = (self.detailResult!.division == "") ? "N/A" : self.detailResult!.division;
                    }
                }
            }
            else if (indexPath.section == OFFICIAL_TIME_SECTION && self.detailResult != nil)
            {
                let timeCell = tableView.dequeueReusableCell(withIdentifier: "officialTimeCell", for: indexPath)  as! OfficialTimeTableViewCell;
                
                timeCell.labelOfficialTime?.text = self.detailResult!.chipTime.replacingOccurrences(of: "1", with: " 1");
                // timeCell.textLabel?.font = UIFont(name: "DBLCDTempBlack", size: 45)!;
                
                return timeCell;
            }
            else if (indexPath.section == RANKING_SECTION && self.detailResult != nil)
            {
                if (indexPath.row == 0)
                {
                    cell?.textLabel?.text = "Overall";
                    cell?.detailTextLabel?.text = formatRanking(self.detailResult!.overall, self.detailResult!.overallTotal);
                }
                else if (indexPath.row == 1)
                {
                    cell?.textLabel?.text = "Division";
                    cell?.detailTextLabel?.text = formatRanking(self.detailResult!.divisionPlace, self.detailResult!.divisionTotal);
                }
                else if (indexPath.row == 2)
                {
                    cell?.textLabel?.text = "Gender";
                    cell?.detailTextLabel?.text = formatRanking(self.detailResult!.genderPlace, self.detailResult!.genderTotal);
                }
            }
            else {
                if (!dataArray.isEmpty) {
                    cell?.textLabel?.text = columnArray[indexPath.row].replacingOccurrences(of: "\\'", with: "'");
                    cell?.detailTextLabel?.text = dataArray[indexPath.row].replacingOccurrences(of: "\\'", with: "'");
                }
            }

            return cell!;
        }
    }

    public func quickResult(_ newResult: QuickResult)
    {
        quickResult = newResult;
    }

    @IBAction func follow(_ sender: Any?)
    {
        if (self.detailResult == nil)
        {
            self.showMessage("Please wait until data is loaded...");
            return;
        }
    
        self.add1Follower(detailResult!);
    }

    @IBAction func addFavorite(_ sender: Any?)
    {
        if (self.detailResult == nil)
        {
            self.showMessage("Please wait until data is loaded...");
            return;
        }
        
        _ = self.add1Favorite(detailResult!);
    }

    @IBAction func remove(_ sender: Any?)
    {
        if (self.detailResult == nil)
        {
            self.showMessage("Please wait until data is loaded...");
            return;
        }
        
        _ = GeminiDatabase().removeFromBookmarks(self.detailResult!.ID);
        
        if (self.detailResult!.follow_id != 0) {
            self.delete1Follower(self.detailResult!.follow_id);
            removeFollow        = true;
            self.detailResult!.follow_id  = 0;
        }
        
        self.navigationController?.popViewController(animated: true);
    }

    @IBAction func send(_ sender: Any?)
    {
        if (self.detailResult == nil)
        {
            self.showMessage("Please wait until data is loaded...");
            return;
        }
        
        let body      = self.generateSocialMediaBody(columnArray, data: dataArray, detailResult: self.detailResult!);
        UIPasteboard.general.string = body;
        
        let activityCharts = GeminiActivity("gemini.charts", title: "Charts", imageName: "Bar Chart-50.png");
        
        let activityVC = UIActivityViewController(activityItems: [body], applicationActivities: [activityCharts]);
        
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.print, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.saveToCameraRoll];
        
        activityVC.completionWithItemsHandler = {(activityType, completed, returnedItems, activityError) in
            if (completed)
            {
                if (activityType == UIActivity.ActivityType.copyToPasteboard)
                {
                    self.showMessage("Copied");
                }
                else if (activityType == activityCharts.activityType/*"gemini.charts"*/) {
                    self.performSegue(withIdentifier: "chartSegue2", sender: nil);
                }
            }
        };
    
        if (self.iPad)
        {
            activityVC.popoverPresentationController?.barButtonItem = sender as? UIBarButtonItem;
        }
    
        self.present(activityVC, animated: true, completion:nil);
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (dataArray.isEmpty)
        {
            return;
        }
    
        if (segue.identifier == "chartSegue2")
        {
            self.chartSelectionTableViewController = segue.destination as? ChartSelectionTableViewController;
            self.chartSelectionTableViewController?.tableName(detailResult!.tableName);
            self.chartSelectionTableViewController?.title = detailResult!.raceTitle;
        }
        //#ifdef YOUTUBE
        //    else if ([segue.identifier isEqualToString: "youTubeSegue"])
        //    {
        //        self.ytPlayerViewController = [segue destinationViewController];
        //        [self.ytPlayerViewController setTitle: athleteName];
        //    }
        //#endif
    }
}
