//
//  EventSubrace.swift
//  Gemini
//
//  Created by Osamu Chiba on 6/15/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import UIKit

class EventSubrace: NSObject {
    var displayName: String = "";
    var tableName: String   = "";
    var ID: Int;
    var postID: Int?;
    
    init(_ pk: Int)
    {
        ID = pk;
        super.init();
    }
}
