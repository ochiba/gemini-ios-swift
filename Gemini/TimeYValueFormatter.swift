import UIKit
import Foundation;
import Charts

class TimeYValueFormatter: NumberFormatter {

    override func string(from number: NSNumber) -> String
    {
        let h: Int = Int(number.intValue / 3600);
        let m: Int = Int(number.intValue / 60 % 60);
        let s: Int = Int(number.intValue % 60);
        
        return String(format: "%02ld:%02ld:%02ld", h, m, s);
    }
}

class ParticipantsValueFormatter : NSObject, IValueFormatter {

    
    func stringForValue(_ value: Double,
                        entry: ChartDataEntry,
                        dataSetIndex: Int,
                        viewPortHandler: ViewPortHandler?) -> String {
        return String(format: "%ld", Int(value));
    }
}
