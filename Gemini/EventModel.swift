//
//  EventModel.swift
//  Gemini
//
//  Created by Osamu Chiba on 6/15/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import UIKit
protocol EventModelProtocol: class {
    var _downloadedData: NSMutableData?;
    func dataDownloaded (_ downloadedData: NSMutableData);
}

class EventModel: NSObject {
    weak var delegate: EventModelProtocol?;

    func downloadEvents()
    {
        // Download the json file
        //http://www.gemininext.com//wordpress-content/plugins/Zekken_Cart/grr_mobile.php
        
        requestURL("https://gemininext.com/mobile/?action=1");
    }
    
    func downloadColumnsOrSubraces(latestID: Int, columnORSubRace bColumn: Bool)
    {
        let bCol:Int = bColumn ? 3 : 4;
        let url: String = "https://gemininext.com/mobile/?action=\(bCol)&latest_id=\(latestID)";
        
        requestURL(url);
    }
    
    func downloadDivisions(_ tableName: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=5&table=\(tableName)";
        
        requestURL(url);
    }
    
    func downloadAllColumnsIn1Table(tableName: String, time_only bTimeOnly: Bool)
    {
        let timeOnly: Int = bTimeOnly ? 1 : 0;
        let url: String = "https://gemininext.com/mobile/?action=10&table=\(tableName)&time_only=\(timeOnly)";
        
        requestURL(url);
    }
    
    func downloadFinishDistributionIn1Table(_ tableName: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=16&table=\(tableName)";
        
        requestURL(url);
    }
    
    func download1MonthOfEvents(year: Int, startMonth month: Int)
    {
        let url: String = "https://gemininext.com/mobile/?action=6&year=_\(year)&month=\(month)";
        
        requestURL(url);
    }
    
    func downloadRacesIn1Event(_ post_id: Int)
    {
        // Download the json file
        
        let url: String = "https://gemininext.com/mobile/?action=7&post_id=\(post_id)";
        NSLog("%", url);
        requestURL(url);
    }
    
    func downloadDivisionDistribution(_ tableName: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=15&table=\(tableName)";
        requestURL(url);
    }
    
    func downloadRacesIn1EventByLoggingIn(userName: String, password pass_word: String)
    {
        // Download the json file
        
        let url: String = "https://gemininext.com/mobile/?action=14&user=\(userName)&password=\(pass_word)";
        NSLog("%", url);
        requestURL(url);
    }
    
    // This is called in the Search View.  Up to 3 fields can be selected besides name, overall, and bib number.
    // For iPhone, only 3 are selected.  For iPad, 5.
    func downloadResults(table_name: String, keyword newKeyword: String, exact bExact: Bool, byDivision useDivision: Bool, displayColumn1  display1: String, displayColumn2 display2: String, displayColumn3 display3: String, displayColumn4 display4: String, displayColumn5  display5: String, isTop3 top3: Bool)
    {
        let nExact: Int         = bExact ? 1 : 0;
        let nUseDivision: Int   = useDivision ? 1 : 0;
        let nTop3: Int   = top3 ? 1 : 0;
        
        
        // Download the json file
        let url: String = "https://gemininext.com/mobile/?action=2&table=\(table_name)&key_word=\(newKeyword)&exact=\(nExact)&byDivision=\(nUseDivision)&displayColumn1=\(display1)&displayColumn2=\(display2)&displayColumn3=\(display3)&displayColumn4=\(display4)&displayColumn5=\(display5)&top3=\(nTop3)";
        NSLog("%", url);
        requestURL(url);
    }
    
    func download1Result(table_name: String, firstName first_name: String, lastName last_name: String, bibNumber bib_number: Int)
    {
        // Download the json file
        
        let url: String = "https://gemininext.com/mobile/?action=8&table=\(table_name)&first_name=\(first_name)&last_name=\(last_name)&bib_number=\(bib_number)";
        requestURL(url);
    }
    
    func download1ResultByKeyword(table_name: String, keyword key_word: String)
    {
        // Download the json file
        
        let url: String = "https://gemininext.com/mobile/?action=19&table=\(table_name)&keyword=\(key_word)";
        requestURL(url);
    }
    
    func add1Follower(table_name: String, bibNumber bib_number: Int, token newToken: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=11&table=\(table_name)&bib_number=\(bib_number)&token=\(newToken)";
        requestURL(url);
    }
    
    func updateToken(oldToken: String, newToken new_token: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=18&old_token=\(oldToken)&new_token=\(new_token)";
        requestURL(url);
    }
    
    func delete1Follower(_ follow_id: Int)
    {
        // Download the json file
        
        let url: String = "https://gemininext.com/mobile/?action=12&follow_id=\(follow_id)";
        requestURL(url);
    }
    
    func downloadUniquePostsForBookmarks(_ post_ids: String)
    {
        let url: String = "https://gemininext.com/mobile/?action=9&post_ids=\(post_ids)";
        requestURL(url);
    }
    
    func getBackgroundImageID()
    {
        requestURL("https://gemininext.com/mobile/?action=17");
    }
    
    func getBackgroundImage(_ imageName: String)
    {
        let url: String = "https://gemininext.com/wp-content/plugins/GeminiRaceResults/images/bw_background/\(imageName)";
        requestURL(url);
    }
    
    func requestURL(_ url: String)
    {
        let jsonFileUrl = URL(string: url);
        
//        NSURL *jsonFileUrl = [NSURL URLWithString:[url stringByAddingPercentEncodingWithAllowedCharacters: [NSCharacterSet URLFragmentAllowedCharacterSet]];
        
//        _downloadedData = [NSMutableData];
        
//        let session = URLSession.shared;
//        NSURLSessionDataTask *downloadTask = [session dataTaskWithURL:jsonFileUrl
//        completionHandler:^(NSData *data,
//        NSURLResponse *response,
//        NSError *error)
//        {
//        [_downloadedData appendData:data;
//        
//        // Ready to notify delegate that data is ready and pass back events
//        if (self.delegate)
//        [self.delegate dataDownloaded: _downloadedData;
//        };
        
//        [downloadTask resume;
        
        let task = URLSession.shared.dataTask(with: jsonFileUrl!) {
            data, response, error in

            if error != nil {
                print(error!.localizedDescription)
                DispatchQueue.main.sync(execute: {
                    AWLoader.hide()
                })

                return
            }

            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? NSArray

                if let parseJSON = json {
                    var items = self.categoryList

                    items.append(contentsOf: parseJSON as! [String])

                    if self.fromIndex < items.count {
                        self.categoryList = items
                        self.fromIndex = items.count

                        DispatchQueue.main.async(execute: {
                            self.categoriesTableView.reloadData()
                            AWLoader.hide()
                        })
                    }
                    else if( self.fromIndex == items.count){
                        DispatchQueue.main.async(execute: {
                            AWLoader.hide()
                        })
                    }
                }
            } catch {
                AWLoader.hide()
                print(error)
            }
        }

        task.resume()
    }
}
