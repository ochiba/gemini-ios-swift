import UIKit

class HeaderButton: UIButton {
    var name: String    = "";
    var ascending: Bool = false;
}
