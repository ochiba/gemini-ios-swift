//
//  AppDelegate.swift
//  Gemini
//
//  Created by Osamu Chiba on 6/14/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseMessaging
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import UserNotifications

// R = 176, G = 31, B = 36
// Hex: B01F24
let geminiRed                   = UIColor(red: 0.69, green: 0.12, blue: 0.14, alpha: 1.0);
let HTTPHeaderField_ContentType = "Content-Type"
let ContentType_ApplicationJson = "application/json"
let HTTPMethod_Post             = "POST"
let DeviceTokenKey              = "DeviceToken";
let gcmMessageIDKey             = "gcm.message_id";

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var ref: DatabaseReference!;

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        ref = Database.database().reference();
        
        changeNavigationColor(true);
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions);
    }

    func firMessageDelegate() {
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self as MessagingDelegate
        UIApplication.shared.registerForRemoteNotifications();
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp();
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication, openURL url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url as URL, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    // Change the navigation color to sort of teal.  Text -> white
    func changeNavigationColor(_ bGemini: Bool)
    {
        UINavigationBar.self.appearance().barTintColor = bGemini ? geminiRed : UIColor.white;
            
        let attributes = [NSAttributedString.Key.foregroundColor : bGemini ? UIColor.white : UIColor.black];
            
        UINavigationBar.self.appearance().titleTextAttributes = attributes;
    }
    
    func deviceToken() -> String
    {
        var token = UserDefaults.standard.string(forKey: DeviceTokenKey);
    
        if (token == nil || (token == "")) {
            InstanceID.instanceID().instanceID { (result, error) in
                if let error = error {
                    print("Error fetching remote instange ID: \(error)")
                } else if let result = result {
                    print("Remote instance ID token: \(result.token)")
                    self.deviceToken(result.token);
                    token = result.token
                }
            }
        }
    
        return token!
    }
    
    func deviceToken(_ newToken: String)
    {
        UserDefaults.standard.set(newToken, forKey: DeviceTokenKey);
    }
    
    public func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("1 Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("2 Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
//    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
//    
//        if (url.path == "/profile") {
//            
//            // switch to profile view controller
//            self.tabBarController.selectedViewController(profileViewController);
//        
//            // pass profileID to profile view controller
//            profileViewController.loadProfile: url.query];
//        }
//        return true;
//    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
   
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        // Print message ID.
        print("3 Message ID: \(userInfo[gcmMessageIDKey]!)")
        print(userInfo)
        completionHandler(.alert);
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo

        if let messageID = userInfo[gcmMessageIDKey] {
            print("4 Message ID: \(messageID)")
        }
        print(userInfo)

        completionHandler()
    }
}

extension AppDelegate : MessagingDelegate {
    /// The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }

    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        self.deviceToken(fcmToken);
    }
}
