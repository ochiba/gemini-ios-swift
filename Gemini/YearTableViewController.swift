//
//  YearTableViewController.swift
//  
//
//  Created by Osamu Chiba on 6/15/17.
//
//

let SECTION_LATEST      = 0;
let SECTION_ARCHIVES    = 1;

import UIKit
import Firebase
import FirebaseDatabase

class YearTableViewCell: UITableViewCell {
    @IBOutlet weak var labelEvent: UILabel?
    @IBOutlet weak var labelDate: UILabel?
    @IBOutlet weak var labelCityState: UILabel?    
}

class YearTableViewController: GeminiTableViewController {

    weak var raceTableViewController: RaceTableViewController?;
    weak var monthTableViewController: MonthTableViewController?;
    var backgroundImage : UIImage?;
    var arrayYears          = [String]();
    var arrayLatestEvents   = [Event]();
    
    override func viewDidLoad() {
        super.viewDidLoad();

        // var ref = FIRDatabase.database().reference(withPath: "wp-posts")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        startDownloading();
    }
    
    override func startDownloading() {
        super.startDownloading();
        downloadEvents();
    }
    
    private func getBackgroundImageID() {
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=17")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 60.0 // TimeoutInterval in Second
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil{
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String];
                
                let random = Int(arc4random_uniform(UInt32(json.count - 1)));
                var imageName:String?;
                
                for index in 0...(json.count - 1) {
                    
                    if (index == random) {
                        imageName = json[index];
                        break;
                    }
                }
                self.getBackgroundImage(imageName!);
            } catch {
                print("Error -> \(error)")
            }
        }
        
        task.resume();
    }
    
    private func getBackgroundImage(_ imageName : String) {
        
        let imageURL = URL.init(string: "https://gemininext.com/wp-content/plugins/GeminiRaceResults/images/bw_background/\(imageName)")
        let imageData = try? Data(contentsOf: imageURL!);
        
        backgroundImage = UIImage(data: imageData!);
        
        DispatchQueue.main.async {
            let tempImageView               = UIImageView(image : self.backgroundImage);
            tempImageView.contentMode       = UIView.ContentMode.scaleAspectFill;
            tempImageView.bounds            = self.tableView.bounds;
            self.tableView.backgroundView   = tempImageView;
        }
        self.dataDownloaded();
    }

    private func downloadEvents() {
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=1")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let jsonEvents = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                if (jsonEvents.count > 0) {
                    
                    self.arrayLatestEvents.removeAll();
                    self.arrayYears.removeAll();
                    
                    for index in 0...(jsonEvents.count - 1) {
                        
                        if (index == 0) {
                            let yearArray = jsonEvents[index] as! NSArray
                            
                            for j in 0...(yearArray.count - 1) {
                                let tempo2 = yearArray[j] as! NSArray
                                self.arrayYears.append(tempo2[0] as! String);
                            }
                        }
                        else {
                            let jsonElement = jsonEvents[index] as! [String : AnyObject];
                            
                            let primaryKey      = Int(jsonElement["ID"] as! String);
                            let oneEvent        = Event(primaryKey!);
                            oneEvent.post_date  = jsonElement["post_date"] as! String;
                            oneEvent.post_title = jsonElement["post_title"] as! String;
                            oneEvent.city       = (jsonElement["_grr_race_location_city"] as? String)!;
                            oneEvent.state      = (jsonElement["_grr_race_location_state"] as? String)!;
                            
                            self.arrayLatestEvents.append(oneEvent);
                        }
                    }
                }
                self.getBackgroundImageID();
                self.dataDownloaded();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        
        task.resume();
    }
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section)
        {
            case SECTION_LATEST:
                return arrayLatestEvents.count;
                
            case SECTION_ARCHIVES:
                return arrayYears.count;
        
            default:
                break;
        }
        return 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String
    {
        return (section == SECTION_LATEST) ? "Latest Results" : "";
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator);
        
//        coordinator.animate(alongsideTransition: nil, completion: {
//            _ in
//            
//            let controller = self.revealViewController().rightViewController
//            
//            var frame = controller?.view.frame
//            frame?.size.height = UIScreen.main.bounds.size.height - self.navigationController!.navigationBar.frame.size.height - self.toolBar.frame.size.height - (UIApplication.shared.isStatusBarHidden ? 0 : 20)
//            controller?.view.frame = frame!
//        })
        
    }

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.section == SECTION_ARCHIVES)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "yearCell", for: indexPath)
            
            cell.backgroundColor = UIColor.clear;

            if (!arrayYears.isEmpty) {

                cell.textLabel?.text = arrayYears[indexPath.row];
                cell.textLabel?.attributedText = NSAttributedString(string: arrayYears[indexPath.row], attributes: [ NSAttributedString.Key.strokeColor : UIColor.black, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strokeWidth : -2 ]);
            }
            return cell;
        }
        else {
            let cell        = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! YearTableViewCell;
            
            cell.backgroundColor = UIColor.clear;
            
            if (!arrayLatestEvents.isEmpty) {
                let oneEvent    = arrayLatestEvents[indexPath.row];
                                
                cell.labelEvent?.attributedText = NSAttributedString(string: oneEvent.post_title, attributes: [ NSAttributedString.Key.strokeColor : UIColor.black, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strokeWidth : -2 ]);
                
                cell.labelCityState?.attributedText = NSAttributedString(string:  oneEvent.city + ", " + oneEvent.state, attributes: [ NSAttributedString.Key.strokeColor : UIColor.black, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strokeWidth : -2 ]);
                
                cell.labelDate?.attributedText = NSAttributedString(string: formatDate(oneEvent.post_date),  attributes: [ NSAttributedString.Key.strokeColor : UIColor.black, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strokeWidth : -2 ]);
                }
            return cell;
        }
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view : UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView;
    
        header.textLabel?.font               = UIFont.boldSystemFont(ofSize: iPad ? 30 : 20);
        header.textLabel?.frame              = header.frame;
        
        header.textLabel?.attributedText = NSAttributedString(string: (section == SECTION_LATEST) ? "Latest Results" : "", attributes: [NSAttributedString.Key.strokeColor : UIColor.black, NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.strokeWidth : -2]);
        header.textLabel?.textAlignment      = .center;
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        
        // One of latest events was selected.
        if (segue.identifier == "raceSegue") {
            
            let oneEvent = arrayLatestEvents[(indexPath?.row)!];

            self.raceTableViewController                = segue.destination as? RaceTableViewController;
            self.raceTableViewController?.currentEvent  = oneEvent;
            self.raceTableViewController?.title         = oneEvent.post_title;
            self.raceTableViewController?.future(false);
        }
        // Year
        else
        {
            self.monthTableViewController = segue.destination as? MonthTableViewController;
            
            let year = arrayYears[(indexPath?.row)!];
            self.monthTableViewController?.year = Int(year)!;
            self.monthTableViewController?.title = year;
        }
    }
   
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if (indexPath.section == SECTION_ARCHIVES)
        {
            if (iPad) {
                return 60;
            }
            else {
                return 44;
            }
        }
        else {
            return 90;
        }
    }
}
