//
//  AboutTableViewController.swift
//  Gemini
//
//  Created by GEMINI on 6/27/17.
//  Copyright (c) 2017 GEMINI. All rights reserved.
//

let ABOUT_VERSION   = 0
let ABOUT_CONTACT   = 1
let ABOUT_CREDITS   = 2
let ABOUT_DEVELOPER = 3

import UIKit

class AboutTableViewController : UITableViewController
{
    override func viewDidLoad() {
        super.viewDidLoad();
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        
        self.tableView.dataSource   = self
        self.tableView.delegate     = self;
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 4;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch (section)
        {
            case ABOUT_CONTACT:
                return 3;
                
            case ABOUT_CREDITS:
                return 2;
            
            case ABOUT_DEVELOPER:
                return 3;
            
            default:
                break;
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String
    {
        switch (section) {
            case ABOUT_VERSION:
                return "Version:";

            case ABOUT_CONTACT:
                return "Contact:";
                
            case ABOUT_CREDITS:
                return "Credits";
            
            case ABOUT_DEVELOPER:
                return "Developer";
            
            default:
                break;
        }
        return "";
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        switch (indexPath.section) {
            
        case ABOUT_CONTACT:
            
            if (indexPath.row == 1) {
                let phoneNumber = URL(string: "tel://6193631501")!
                UIApplication.shared.open(phoneNumber, options: [:], completionHandler: nil)
            }
            else if (indexPath.row == 2) {
                let tempo   =  "mailto:support@gemininext.com?Subject=Yo!  Something is not right!";
                let email   = tempo.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed);
                let url     = URL(string: email!);
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
            }
            break;
            
        case ABOUT_VERSION:
            break;
            
        case ABOUT_CREDITS:
            switch (indexPath.row) {
            case 0:
                let url = URL(string: "http://icons8.com");
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)

                break;
                
            case 1:
                let url = URL(string: "https://github.com/danielgindi/Charts");
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                break;
                
            default:
                break;
            }
            break;
        
        case ABOUT_DEVELOPER:
            switch (indexPath.row) {
            case 1:
                let tempo   = "mailto:osamu@opix.net?Subject=So..., you are the developer...";
                let email   = tempo.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed);
                let url     = URL(string: email!);

                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                break;
                
            case 2:
                let url = URL(string: "http://www.opix.net");
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                break;
                
            default:
                break;
            }
            break;

        default:
            break;
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = indexPath.section == ABOUT_VERSION ? "versionCell" : "AboutCell";
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        
        switch (indexPath.section) {
            
            case ABOUT_CONTACT:
     
                if (indexPath.row == 0) {
                    cell.textLabel?.text = "Gemini Next";
                }
                else if (indexPath.row == 1) {
                    cell.textLabel?.text                        = "(619)363-1501";
                    cell.textLabel?.isUserInteractionEnabled    = true;
                    cell.textLabel?.textColor                   = UIColor.blue;
                }
                else if (indexPath.row == 2) {
                    cell.textLabel?.text                        = "support@gemininext.com";
                    cell.textLabel?.isUserInteractionEnabled    = true;
                    cell.textLabel?.textColor                   = UIColor.blue;
                }

                break;
                
            case ABOUT_VERSION:
                break;
                
            case ABOUT_CREDITS:
                cell.textLabel?.isUserInteractionEnabled = true;
                
                switch (indexPath.row) {
                    case 0:
                        cell.textLabel?.text = "http://icons8.com";
                        break;
                        
                    case 1:
                        cell.textLabel?.text = "https://github.com/danielgindi/Charts";
                        break;
                        
                    default:
                        break;
                }
                
                cell.textLabel?.textColor = UIColor.blue;
                break;
            
            case ABOUT_DEVELOPER:
                cell.textLabel?.isUserInteractionEnabled = indexPath.row != 0;
                
                if (indexPath.row != 0) {
                    cell.textLabel?.textColor = UIColor.blue;
                }
                
                switch (indexPath.row) {
                    case 0:
                        cell.textLabel?.text = "Osamu Chiba";
                        break;
                
                    case 1:
                        cell.textLabel?.text = "osamu@opix.net";
                        break;

                    case 2:
                        cell.textLabel?.text = "www.opix.net";
                        break;

                    default:
                        break;
                }
            
                break;

            default:
                break;
        }
        
        return cell;
    }
}
