import UIKit
class MonthTableViewController : UITableViewController {
    
    var year: Int   = 2017;
    let arrayMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
    weak var oneMonthTableViewController: OneMonthTableViewController?;

    
    override func viewDidLoad() {
        super.viewDidLoad();
     }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayMonths.count;
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false;
    }

    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "monthCell", for: indexPath)
        cell.textLabel?.text = arrayMonths[indexPath.row]
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        
        self.oneMonthTableViewController = segue.destination as? OneMonthTableViewController;
        self.oneMonthTableViewController?.setYearAndMonth(year, newMonth: (indexPath?.row)! + 1);
        self.oneMonthTableViewController?.title = "\(arrayMonths[(indexPath?.row)!]) \(year)";
    }
}
