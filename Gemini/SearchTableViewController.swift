import UIKit

let HEADER_HEIGHT   = 65.0;

let SEARCH_EXACT    = 0
let SEARCH_PARTIAL  = 1

let ViewControllerTitleKey          = "ViewControllerTitleKey";
let SearchControllerIsActiveKey     = "SearchControllerIsActiveKey";
let SearchBarTextKey                = "SearchBarTextKey";
let SearchBarIsFirstResponderKey    = "SearchBarIsFirstResponderKey";

class SearchTableViewController  : GeminiTableViewController, UISearchBarDelegate, UISearchControllerDelegate, UISearchResultsUpdating {

    var tableName = "";
    var display3 = "", display2 = "", display1 = "", display4 = "", display5 = "", currentKeyword = ""
    var arraySearchResults = [QuickResult]()
    var arrayDivisions      = [String]()
    var currentResult: QuickResult?;

    var bSearching          = false;
    var socialMediaIndex    = false;
    var bDivision           = false;
    var top3                = false;
    var chart               = false;
    var bFromColumnTableView = false;
    var searchControllerWasActive = false;
    var searchControllerSearchFieldWasFirstResponder = false;
    
    weak var columnTableViewController: ColumnTableViewController?;
    weak var chartSelectionTableViewController: ChartSelectionTableViewController?;
    weak var oneResultTableViewController: OneResultTableViewController?;

    var headerTableViewCell: HeaderTableViewCell?;
    var searchController: UISearchController?;

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = arraySearchResults.count
        return count;
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60;
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if (self.future())
        {
            return 44.0;
        }
        return CGFloat(HEADER_HEIGHT);
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool  {
        return true;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("section = \(indexPath.section)")
        print("row = \(indexPath.row)")
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "oneResultCell", for: indexPath) as? OneResultTableViewCell else {
            return UITableViewCell()
        }
        
        if (arraySearchResults.count > 0)
        {
            let oneResult  = arraySearchResults[indexPath.row]

            cell.labelName?.text     = "\(oneResult.firstName) \(oneResult.lastName) (#\(oneResult.bibNumber))";
            
            // Once award is given to a certain individual, cross out.
            if (oneResult.awarded != TOP3_NOT_RECEIVED)
            {
                let attributeString = NSMutableAttributedString(string: (cell.labelName?.text)!);
                attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                
                attributeString.addAttribute(NSAttributedString.Key.strikethroughColor, value: (oneResult.awarded == TOP3_RECEIVED) ? UIColor.blue : UIColor.red, range: NSMakeRange(0, attributeString.length))
                
                cell.labelName?.attributedText = attributeString;
            }
            
            // For future events, overall does not mean anything so leave it blank.
            if (!self.future()) {
                cell.labelPlace?.text    = "\(oneResult.overall)";
            }
            
            cell.labelDisplay1?.text = oneResult.display1;
            cell.labelDisplay2?.text = oneResult.display2;
            cell.labelDisplay3?.text = oneResult.display3;
            
            if (iPad)
            {
                cell.labelDisplay4?.text = oneResult.display4;
                cell.labelDisplay5?.text = oneResult.display5;
            }
            
            // For future, don't show counter.
            cell.labelCounter?.text  = self.future() ? "" : "\(indexPath.row + 1)";
            
            // Change row color alternately
            if (indexPath.row % 2 != 0) {
                cell.backgroundColor = lightGray
            }
            else {
                cell.backgroundColor = UIColor.white;
            }
        }
        return cell;
    }

    @IBAction func changeDisplayColumn(_ sender: Any?)
    {
        chart = false;
        self.performSegue(withIdentifier: "displaySegue", sender: sender);
    }
    
    @IBAction func showTimeColumns(_ sender: Any?)
    {
        chart = true;
        self.performSegue(withIdentifier: "displaySegue", sender:sender);
    }

    @IBAction func sortByDisplay1(_ sender: Any?)
    {
        self.setSortBy(DISPLAY_1);
    }
    
    @IBAction func sortByDisplay2(_ sender: Any?)
    {
        self.setSortBy(DISPLAY_2);
    }
    
    @IBAction func sortByDisplay3(_ sender: Any?)
    {
        self.setSortBy(DISPLAY_3);
    }
    
    @IBAction func sortByDisplay4(_ sender: Any?)
    {
        self.setSortBy(DISPLAY_4);
    }
    
    @IBAction func sortByDisplay5(_ sender: Any?)
    {
        self.setSortBy(DISPLAY_5);
    }
    
    @IBAction func sortByOverall(_ sender: Any?)
    {
        self.setSortBy(iPad ? DISPLAY_OVERALL : DISPLAY_4);
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }

    func setSortBy(_ sortColumn: Int)
    {
        let tempoHeader = self.headerTableViewCell?.titleAtIndex(sortColumn);
    
        if (tempoHeader == "")
        {
            return;
        }
        
        let button      = self.headerTableViewCell?.setNewCurrentSelection(sortColumn);
        let ascending   = button?.ascending ?? true
        let column      = button?.name ?? "display1"

        if (iPad) {
            sort(column, ascending)
        } else {
            sort((sortColumn == DISPLAY_4) ? "overall" : column, ascending)
        }
    
        DispatchQueue.main.async {
            self.tableView.reloadData();
        }
    }

    func sort(_ key: String, _ ascending: Bool) {

        if key.compare("display1") == .orderedSame {
            if ascending {
                self.arraySearchResults.sort(by: { $0.display1 < $1.display1 })
            } else {
                self.arraySearchResults.sort(by: { $0.display1 > $1.display1 })
            }
        } else if key.compare("display2") == .orderedSame {
            if ascending {
                self.arraySearchResults.sort(by: { $0.display2 < $1.display2 })
            } else {
                self.arraySearchResults.sort(by: { $0.display2 > $1.display2 })
            }
        } else if key.compare("display3") == .orderedSame {
            if ascending {
                self.arraySearchResults.sort(by: { $0.display3 < $1.display3 })
            } else {
                self.arraySearchResults.sort(by: { $0.display3 > $1.display3 })
            }
        } else if key.compare("display4") == .orderedSame {
            if ascending {
                self.arraySearchResults.sort(by: { $0.display4 < $1.display4 })
            } else {
                self.arraySearchResults.sort(by: { $0.display4 > $1.display4 })
            }
        } else if key.compare("display5") == .orderedSame {
            if ascending {
                self.arraySearchResults.sort(by: { $0.display5 < $1.display5 })
            } else {
                self.arraySearchResults.sort(by: { $0.display5 > $1.display5 })
            }
        } else {
            if ascending {
                self.arraySearchResults.sort(by: { $0.overall < $1.overall })
            } else {
                self.arraySearchResults.sort(by: { $0.overall > $1.overall })
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar){}
    func didDismissSearchController(_ searchController: UISearchController) {}
    func willPresentSearchController(_ searchController: UISearchController) {}
    func didPresentSearchController(_ searchController: UISearchController) {}
    func willDismissSearchController(_ searchController: UISearchController) {}
    func presentSearchController(_ searchController: UISearchController) {}

    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return nil;
    }
    
    override func encodeRestorableState(with coder: NSCoder)
    {
        super.encodeRestorableState(with: coder);
        
        coder.encode(self.title, forKey: ViewControllerTitleKey);
        
        let searchController = self.searchController;
        
        // encode the search controller's active state
        coder.encode(searchController?.isActive, forKey: SearchControllerIsActiveKey);
        
        // encode the first responser status
        if (searchController?.isActive)! {
            coder.encode(searchController?.searchBar.isFirstResponder, forKey:SearchBarIsFirstResponderKey);
        }
        
        // encode the search bar text
        coder.encode(searchController?.searchBar.text, forKey:SearchBarTextKey);
    }

    override func decodeRestorableState(with coder: NSCoder)
    {
        super.decodeRestorableState(with: coder);
    
        // restore the title
        self.title = coder.decodeObject(forKey: ViewControllerTitleKey) as? String;
        
        // restore the active state:
        // we can't make the searchController active here since it's not part of the view
        // hierarchy yet, instead we do it in viewWillAppear
        //
        self.searchControllerWasActive = coder.decodeBool(forKey: SearchControllerIsActiveKey);
        
        // restore the first responder status:
        // we can't make the searchController first responder here since it's not part of the view
        // hierarchy yet, instead we do it in viewWillAppear
        //
        self.searchControllerSearchFieldWasFirstResponder = coder.decodeBool(forKey: SearchBarIsFirstResponderKey);
        
        // restore the text in the search field
        self.searchController?.searchBar.text = coder.decodeObject(forKey: SearchBarTextKey) as? String;
    }

    func updateSearchResults(for searchController: UISearchController)
    {
    /*// update the filtered array based on the search text
     currentKeyword = searchController.searchBar.text;
     
     bDivision = false; // Always set to NO.  Set to Yes only in clickedButtonAtIndex().
     
     self.startDownloading();
     */
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        // restore the searchController's active state
        if (self.searchControllerWasActive) {
            self.searchController?.isActive = self.searchControllerWasActive;
            self.searchControllerWasActive = false;
            
            if (self.searchControllerSearchFieldWasFirstResponder) {
                self.searchController?.searchBar.becomeFirstResponder();
                self.searchControllerSearchFieldWasFirstResponder = false;
            }
        }
        
        if (bFromColumnTableView)
        {
            bFromColumnTableView = false;
            
            if (self.columnTableViewController == nil) {
                return;
            }
            
            let tempoDisplay1 = self.columnTableViewController?.display(0);
            let tempoDisplay2 = self.columnTableViewController?.display(1);
            let tempoDisplay3 = self.columnTableViewController?.display(2);
            let tempoDisplay4 = self.columnTableViewController?.display(3);
            let tempoDisplay5 = self.columnTableViewController?.display(4);
            
            // If nothing is selected, then do nothing.
            if (tempoDisplay1 == "")
            {
                return;
            }
            
            // If no change, then do nothing.
            if ((display1 == tempoDisplay1) &&
                (display2 == tempoDisplay2) &&
                (display3 == tempoDisplay3) &&
                (display3 == tempoDisplay4) &&
                (display3 == tempoDisplay5)) {
                return;
            }
            
            headerTableViewCell?.setTitleAtIndex(tempoDisplay1!, index : DISPLAY_1);
            headerTableViewCell?.setTitleAtIndex(tempoDisplay2!, index : DISPLAY_2);
            headerTableViewCell?.setTitleAtIndex(tempoDisplay3!, index : DISPLAY_3);
            
            display1 = tempoDisplay1!;
            display2 = tempoDisplay2!;
            display3 = tempoDisplay3!;
            
            if (iPad)
            {
                display4 = tempoDisplay4!;
                display5 = tempoDisplay5!;
                
                headerTableViewCell?.setTitleAtIndex(tempoDisplay4!, index : DISPLAY_4);
                headerTableViewCell?.setTitleAtIndex(tempoDisplay5!, index : DISPLAY_5);
            }
            
            self.startDownloading();
        }
        
        self.bFromColumnTableView = false;
    }

    @IBAction func filterByDivision(_ sender: Any?)
    {
        if (self.arrayDivisions.isEmpty)
        {
            return;
        }
        
        let alert = UIAlertController(title:"Filter", message: "Choose a Division", preferredStyle: UIAlertController.Style.actionSheet);
        
        for j in 0..<self.arrayDivisions.count
        {
            let division = UIAlertAction(title: arrayDivisions[j], style: UIAlertAction.Style.default, handler: { (action) -> Void in
                
                if (action.title == "Cancel")
                {
                    return;
                }
                
                self.bDivision   = true;
                
                self.currentKeyword = action.title!;
                
                if (self.currentKeyword == "No Filter") {
                    self.currentKeyword = "";
                }
                
                self.top3 = self.currentKeyword == "Top 3 / Division";
                
                // Shared by iPhone and iPad; however, for iPhone, 4 and 5 are always blank.
                self.startDownloading();
            });
            
            alert.addAction(division);
        }
        
        alert.popoverPresentationController?.barButtonItem = (sender as! UIBarButtonItem);
        self.present(alert, animated: true, completion: nil);
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        var keyword = "";
        let bBlankKeyword = (currentKeyword == "");
        
        if (!bBlankKeyword) {
            keyword = " (Keyword: \(currentKeyword))";
        }
        
        var headerRect = tableView.tableHeaderView?.bounds;
        let header = UIView(frame: headerRect!);
        
        // Future events are up to 2 lines.  No column line.
        if (self.future())
        {
            let futureHeaderCell = tableView.dequeueReusableCell(withIdentifier: "futureHeaderCell");
            
            if (arraySearchResults.count != 1) {
                futureHeaderCell?.textLabel?.text   = "\(arraySearchResults.count) Entries\(keyword)";
            }
            else {
                futureHeaderCell?.textLabel?.text   = "1 Entry\(keyword)";
            }
            
            futureHeaderCell?.frame = headerRect!;
            header.addSubview(futureHeaderCell!);
        }
        else
        {
            if (self.headerTableViewCell == nil) {
                self.headerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as? HeaderTableViewCell;
            }
            
            if (arraySearchResults.count != 1)
            {
                self.headerTableViewCell!.labelResults?.text = "\(arraySearchResults.count) Results\(keyword)";
            }
            else
            {
                self.headerTableViewCell!.labelResults?.text = "1 Result\(keyword)";
            }
            
            // Solution to avoid the header move along with a swiped row:
            // http://stackoverflow.com/questions/26009722/swipe-to-delete-cell-causes-tableviewheader-to-move-with-cell
            
            // This is a custom cell and needs to be set to HEADER_HEIGHT; otherwise, its default height (44 or whatever) will be used.
            headerRect?.size.height = CGFloat(HEADER_HEIGHT);
            self.headerTableViewCell!.frame = headerRect!;
            header.addSubview(self.headerTableViewCell!);
        }
        return header;
    }
    
    func downloadForSocialMedia() {

        let escapedLastName = currentResult!.lastName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedFirstName = currentResult!.firstName.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=8&table=\(currentResult!.tableName)&first_name=\(escapedFirstName!)&last_name=\(escapedLastName!)&bib_number=\(currentResult!.bibNumber)")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0 // TimeoutInterval in Second
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let jsonArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                let newData = self.generateDetailResult(column: jsonArray[0] as! [String], data: jsonArray[1] as! [String], quickResult: self.currentResult!);
                
                let columnArray    = newData.columnArray;
                let dataArray      = newData.dataArray;
                let detailResult   = newData.detailResult;

                let body      = self.generateSocialMediaBody(columnArray, data: dataArray, detailResult: detailResult);
                UIPasteboard.general.string = body;
                    
                self.socialMediaIndex    = false;
                
                let activityCharts = GeminiActivity("gemini.charts", title: "Charts", imageName: "Bar Chart-50.png");
                
                let activityVC = UIActivityViewController(activityItems: [body], applicationActivities: [activityCharts]);
                
                activityVC.excludedActivityTypes = [UIActivity.ActivityType.postToWeibo, UIActivity.ActivityType.assignToContact, UIActivity.ActivityType.print, UIActivity.ActivityType.addToReadingList, UIActivity.ActivityType.postToTencentWeibo, UIActivity.ActivityType.saveToCameraRoll];
                
                activityVC.completionWithItemsHandler = {(activityType, completed, returnedItems, activityError) in
                    if (completed)
                    {
                        if (activityType == UIActivity.ActivityType.copyToPasteboard)
                        {
                            self.showMessage("Copied");
                        }
                        else if (activityType == activityCharts.activityType/*"gemini.charts"*/) {
                            self.performSegue(withIdentifier: "chartSegue3", sender: nil);
                        }
                    }
                };
                
                if (self.iPad)
                {
                    activityVC.popoverPresentationController?.sourceView = self.view;
                }
                
                self.enableButtons();
                self.present(activityVC, animated: true, completion:nil);
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }
    
    func downloadDivisions(_ table: String)
    {
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=5&table=\(table)")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0 // TimeoutInterval in Second
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let jsonArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
  
                self.arrayDivisions.removeAll();
                
                for i in 0..<jsonArray.count
                {
                    let jsonElement = jsonArray[i] as! [AnyObject];
                    
                    let oneDivision      = jsonElement[0] as! String;

                    self.arrayDivisions.append(oneDivision);
                }
                
                // Add "No Filter"
                self.arrayDivisions.insert("No Filter", at: 0);
                
                if (!self.future()) {
                    self.arrayDivisions.insert("Top 3 / Division", at: 1);
                }
                
                self.arrayDivisions.append("Cancel");
                
                self.bSearching      = true;
                self.currentKeyword  = "";
                self.startDownloading();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }
    
    func downloadResults(_ table_name: String, keyword newKeyword: String, exact bExact: Bool, byDivision useDivision: Bool, displayColumn1 display1: String, displayColumn2 display2: String, displayColumn3 display3: String, displayColumn4 display4: String,  displayColumn5 display5: String, isTop3 top3: Bool)
    {
        let escapedKeyword = newKeyword.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedDisplay1 = display1.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedDisplay2 = display2.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedDisplay3 = display3.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedDisplay4 = display4.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        let escapedDisplay5 = display5.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed);
        
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=2&table=\(table_name)&key_word=\(escapedKeyword!)&exact=\(bExact ? 1 : 0)&byDivision=\(useDivision ? 1 : 0)&displayColumn1=\(escapedDisplay1!)&displayColumn2=\(escapedDisplay2!)&displayColumn3=\(escapedDisplay3!)&displayColumn4=\(escapedDisplay4!)&displayColumn5=\(escapedDisplay5!)&top3=\(top3 ? 1 : 0)")
        
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                if (self.arraySearchResults.count > 0) {
                    self.arraySearchResults.removeAll();
                }
                
                let jsonArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];

                // This is called by the search view controller.  No details needed:  `Overall`, `Bib`, `First Name`, `Last Name`, `Chip` or `Total Time`.
                // Loop through Json objects, create question objects and add them to our questions array
                for i in 0..<jsonArray.count
                {
                    let jsonElement = jsonArray[i] as! [String];
                    
                    let id = Int(jsonElement[1])!;
                    
                    let oneResult  = QuickResult(id);
                    oneResult.overall       = Int(jsonElement[0])!;
                    
                    oneResult.firstName     = jsonElement[2].replacingOccurrences(of: "\\'", with: "'");
                    oneResult.lastName      = jsonElement[3].replacingOccurrences(of: "\\'", with: "'");
                    
                    // 3 more additional fields can be selected.
                    if (jsonElement.count == 5) {
                        oneResult.display1  = jsonElement[4];
                    }
                    else if (jsonElement.count == 6)
                    {
                        oneResult.display1  = jsonElement[4];
                        oneResult.display2  = jsonElement[5];
                    }
                    else if (jsonElement.count == 7)
                    {
                        oneResult.display1  = jsonElement[4];
                        oneResult.display2  = jsonElement[5];
                        oneResult.display3  = jsonElement[6];
                    }
                    else if (self.iPad)
                    {
                        if (jsonElement.count == 8)
                        {
                            oneResult.display1  = jsonElement[4];
                            oneResult.display2  = jsonElement[5];
                            oneResult.display3  = jsonElement[6];
                            oneResult.display4  = jsonElement[7];
                        }
                        else if (jsonElement.count == 9)
                        {
                            oneResult.display1  = jsonElement[4];
                            oneResult.display2  = jsonElement[5];
                            oneResult.display3  = jsonElement[6];
                            oneResult.display4  = jsonElement[7];
                            oneResult.display5  = jsonElement[8];
                        }
                    }
                    
                    // When sorted, 00:00:00 should not be at the top.
                    if (oneResult.display1 == "00:00:00") {
                        // Future events get blank.
                        oneResult.display1 = self.isFuture ? "" : "N/A";
                    }
                    
                    if (oneResult.display2 == "00:00:00") {
                        oneResult.display2 = "N/A";
                    }
                    
                    if (oneResult.display3 == "00:00:00")
                    {
                        oneResult.display3 = "N/A";
                    }
                    
                    if (oneResult.display4 == "00:00:00") {
                        oneResult.display4 = "N/A";
                    }
                    
                    if (oneResult.display5 == "00:00:00") {
                        oneResult.display5 = "N/A";
                    }
                    
                    oneResult.tableName     = self.tableName;
                    oneResult.raceTitle     = self.title!;
                    
                    self.arraySearchResults.append(oneResult);
                }
                
                // Reset the first column ascending to NO, however, this will be reversed.
                
                if (self.headerTableViewCell == nil) {
                    self.headerTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "headerCell") as? HeaderTableViewCell;
                }

                let button1 = self.headerTableViewCell!.setNewCurrentSelection(DISPLAY_1);
                button1.ascending = false;
                
                // Normally call this function and be done here.  However, N/A should not be at the top of column 1.  Sort by the column and N/A should be at the end.
                self.dataDownloaded();
                self.enableButtons();
                self.sortByDisplay1(nil);
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        self.currentResult  = arraySearchResults[indexPath.row]
        
        if (self.future())
        {
            let followAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Follow", handler: {(action, indexPath) -> Void in
                
                self.add1Follower(self.currentResult!);
                // Refresh the row.  Currently this happens before the action view pops up.
                // Launch reload for the two index path
                self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade);
                self.enableButtons();
            });
            
            followAction.backgroundColor = geminiRed;
            
            return [followAction];
        }
        else
        {
            // This shows all the social media options.
            
            let shareAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Share", handler: {(action, indexPath) -> Void in
                
                self.socialMediaIndex = true;
                
                self.downloadForSocialMedia();
                
                // Refresh the row.  Currently this happens before the action view pops up.
                // Launch reload for the two index path
                self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade);
                self.enableButtons();
            });
            shareAction.backgroundColor = geminiRed;
            
            return [shareAction];
        }
    }

    func enableButtons()
    {
        DispatchQueue.main.async {
            self.navigationItem.rightBarButtonItem?.isEnabled = true; // Filter button.
        }
        
        if (!self.future())
        {
            DispatchQueue.main.async {
                let buttonColumn = self.navigationItem.rightBarButtonItems?[1];
                buttonColumn?.isEnabled = true;
            }
        }
    }
    
    override func startDownloading()
    {
        super.startDownloading();
        
        if (!self.future())
        {
            // During downloading, disable right buttons.
            let buttonColumn = self.navigationItem.rightBarButtonItems?[1];
            buttonColumn?.isEnabled = false;
        }
        
        // Default one (filter in this case)
        self.navigationItem.rightBarButtonItem?.isEnabled = false;
        
        self.downloadResults(tableName, keyword: currentKeyword, exact: false, byDivision: bDivision, displayColumn1: display1, displayColumn2: display2, displayColumn3: display3, displayColumn4: display4, displayColumn5: display5, isTop3: top3);
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;

        if (self.arraySearchResults.count == 0) {
            return;
        }
        
        let oneResult = self.arraySearchResults[(indexPath == nil) ? 0 : (indexPath?.row)!]
        
        if (segue.identifier == "displaySegue")
        {
            // Already called once.
            if (bFromColumnTableView) {
                return;
            }
            self.columnTableViewController = segue.destination as? ColumnTableViewController;

            self.columnTableViewController?.tableName = oneResult.tableName;
            self.columnTableViewController?.setDisplayColumns([display1, display2, display3, display4, display5]);
            
            // Set if the column view is for chart or not.
            // self.columnTableViewController.setChart: chart keyword: currentKeyword division: bDivision top3: top3];
            
            bFromColumnTableView = true;
            return;
        }
        
        if (segue.identifier == "chartSegue3")
        {
            self.chartSelectionTableViewController              = segue.destination as? ChartSelectionTableViewController;
            self.chartSelectionTableViewController?.tableName   = oneResult.tableName;
            self.chartSelectionTableViewController?.title       = oneResult.raceTitle;
            return;
        }
        
        self.oneResultTableViewController = segue.destination as? OneResultTableViewController;
        self.oneResultTableViewController?.quickResult(oneResult);
        self.oneResultTableViewController?.currentEvent(currentEvent!);
        self.oneResultTableViewController?.title = "\(oneResult.firstName) \(oneResult.lastName)";
        self.oneResultTableViewController?.future(self.future());
    }
    // Catch when orientation changes:
    // https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIContentContainer_Ref/index.html#//apple_ref/occ/intfm/UIContentContainer/viewWillTransitionToSize:withTransitionCoordinator:
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator);
        // Need to refresh because the header width changes and therefore the header lines need to be recalculated.
        self.tableView.reloadData();
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.bDivision   = false; // Always set to NO.  Set to Yes only in clickedButtonAtIndex().
        self.top3        = false;
        
        searchBar.viewWithTag(0);
        
        let newKeyword = searchBar.text;
        
        if (newKeyword == "")
        {
            return;
        }
        
        if (currentKeyword == newKeyword)
        {
            return;
        }
        // searchBar.searchActive = false;
        searchBar.resignFirstResponder();
        currentKeyword = newKeyword!;
        
        self.startDownloading();
    }

    override func viewDidLoad()
    {
        super.viewDidLoad();
        
        self.headerTableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as? HeaderTableViewCell;
        self.headerTableViewCell?.initializeWithiPad(iPad);
        
        display1            = COLUMN_CHIP;
        display2            = "";
        display3            = "";
        display4            = "";
        display5            = "";
        
        self.downloadDivisions(tableName);

        self.searchController = UISearchController(searchResultsController: nil);
        self.searchController?.searchResultsUpdater = self;
        self.searchController?.searchBar.sizeToFit();
        self.tableView.tableHeaderView = self.searchController?.searchBar;
        
        // we want to be the delegate for our filtered table so didSelectRowAtIndexPath is called for both tables
        self.searchController?.delegate                              = self;
        self.searchController?.dimsBackgroundDuringPresentation      = false; // default is YES
        self.searchController?.searchBar.delegate                    = self; // so we can monitor text changes + others
        self.searchController?.hidesNavigationBarDuringPresentation  = false;
        self.searchController?.searchBar.placeholder                 = "Enter last name or bib number";
        
        // self.searchController?.searchBar.scopeButtonTitles = @[NSLocalizedString("Exact", "Exact"), NSLocalizedString("Partial", "Partial")];

        // Search is now just presenting a view controller. As such, normal view controller
        // presentation semantics apply. Namely that presentation will walk up the view controller
        // hierarchy until it finds the root view controller or one that defines a presentation context.
        //
        self.definesPresentationContext = true;  // know where you want UISearchController to be displayed
        
        // Add another button if this event is not a future event.
        // The extra button is used to display different column.
        if (!self.future())
        {
            // Get the existing right button array, which already has Filter button.
            let btnArray = self.navigationItem.rightBarButtonItems;
            var arrRightBarItems = btnArray;
        
            // This is one way to add a custom button, but not used here since a system icon is used.
            
            let btnSetting = UIButton(type:UIButton.ButtonType.custom);
            btnSetting.tintColor = UIColor.white;
            btnSetting.setImage(UIImage(named: "Numbered List Filled-50.png"), for: UIControl.State.normal);
            btnSetting.frame = CGRect(x: 0, y: 0, width: 32, height: 32);
            btnSetting.showsTouchWhenHighlighted = false;
            btnSetting.addTarget(self, action: #selector(changeDisplayColumn), for: UIControl.Event.touchUpInside);
            
            let barButtonItem = UIBarButtonItem(customView: btnSetting);
            arrRightBarItems?.append(barButtonItem);

            // Reset the right button array.
            self.navigationItem.rightBarButtonItems = arrRightBarItems;
            
            // During downloading, disable right buttons.
            let buttonColumn = self.navigationItem.rightBarButtonItems?[1];
            buttonColumn?.isEnabled = false;
            
            // Default one (filter in this case)
            self.navigationItem.rightBarButtonItem?.isEnabled = false;
        }
    }
}
