import UIKit

// #define SECTION_INFORMATION         0
let SECTION_INDIVIDUAL_RESULTS  = 0// 1
let SECTION_CHARTS              = 1// 2

class RaceTableViewController : GeminiTableViewController {

    weak var searchTableViewController: SearchTableViewController?;
    weak var chartSelectionTableViewController: ChartSelectionTableViewController?;

    var arrayRaces : NSMutableArray = [];
  
    override func viewDidLoad() {
        super.viewDidLoad();
        self.future(self.isFuture(self.currentEvent!.post_date));
        startDownloading();
    }

    override func startDownloading() {
        super.startDownloading();
        downloadRacesIn1Event();
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayRaces.count;
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String
    {
        return (section == SECTION_CHARTS) ? "Charts" : "Results";
    }

    override func tableView(_ tableView: UITableView, willDisplayHeaderView view : UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView;
        
        header.textLabel?.textColor     = UIColor.darkGray;
        header.textLabel?.font          = UIFont.boldSystemFont(ofSize: 17);
        header.textLabel?.frame         = header.frame;
        header.textLabel?.textAlignment = .center;
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.isFuture ? 1 : 2;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let identifier = (indexPath.section == SECTION_CHARTS) ? "chartCell" : "oneRaceCell";
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        // For non-information sections, arrayRaces must be non-zero.
        if (arrayRaces.count > 0) {
            let oneRace = arrayRaces[indexPath.row] as! EventSubrace;
            cell.textLabel?.text = oneRace.displayName;
            
            // No charts for the look-up tables.
            if (indexPath.section == SECTION_CHARTS)
            {
                if (oneRace.displayName.isEqual("Number Lookup") || oneRace.displayName.isEqual("Confirmation"))
                {
                    cell.isUserInteractionEnabled = false;
                    cell.textLabel?.isEnabled     = false;
                }
            }
        }
        return cell;
    }

    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool  {
        if (identifier.isEqual("chartSegue"))
        {
            if (self.future())
            {
                return false;
            }
            
            let indexPath   = self.tableView.indexPathForSelectedRow;
            let oneRace     = arrayRaces[(indexPath?.row)!] as! EventSubrace;
            
            if (oneRace.displayName.isEqual("Number Lookup") || oneRace.displayName.isEqual("Confirmation"))
            {
                return false;
            }
        }
        return true;
    }

    private func downloadRacesIn1Event() {
        let post_id = currentEvent!.ID;
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=7&post_id=\(post_id)")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0 // TimeoutInterval in Second
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let jsonRaces = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                if (self.arrayRaces.count > 0) {
                    self.arrayRaces.removeAllObjects();
                }
                
                if (jsonRaces.count > 0) {
                
                    for k in 0...(jsonRaces.count - 1) {
                        
                        let jsonElement2 = jsonRaces[k] as! [AnyObject];
                        
                        let primaryKey      = Int(jsonElement2[0] as! String);
                        let oneRace         = EventSubrace(primaryKey!);
                        oneRace.displayName = jsonElement2[1] as! String;
                        oneRace.tableName   = jsonElement2[2] as! String;
                        
                        self.arrayRaces.add(oneRace);
                    }
                }
                self.dataDownloaded();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        let oneRace = arrayRaces[(indexPath?.row)!] as! EventSubrace;
        
        if (indexPath?.section == SECTION_INDIVIDUAL_RESULTS)
        {
            self.searchTableViewController = segue.destination as? SearchTableViewController;
            self.searchTableViewController!.tableName = oneRace.tableName;
            self.searchTableViewController!.title = oneRace.displayName;
            self.searchTableViewController!.currentEvent(self.currentEvent!);
            self.searchTableViewController!.future(self.future());
            
            // Another check if this selection is future, even if the user checks this on the race day.
            if (oneRace.displayName.isEqual("Number Lookup") || oneRace.displayName.isEqual("Confirmation")) {
                self.future(true);
            }
        }
        else // SECTION_CHARTS
        {
            self.chartSelectionTableViewController = segue.destination as? ChartSelectionTableViewController;
            self.chartSelectionTableViewController?.tableName(oneRace.tableName);
            self.chartSelectionTableViewController?.title = oneRace.displayName;
        }
    }
}
