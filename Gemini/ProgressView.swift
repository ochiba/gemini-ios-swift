import UIKit

open class ProgressView {
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    open class var shared: ProgressView {
        struct Static {
            static let instance: ProgressView = ProgressView()
        }
        return Static.instance
    }
    
    open func showProgressView(_ view: UIView) {
        DispatchQueue.main.async {
            self.containerView.frame = view.frame
            self.containerView.center = view.center
            self.containerView.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.3)
            
            self.progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            self.progressView.center = view.center
            self.progressView.backgroundColor = UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 0.8);
            self.progressView.clipsToBounds = true
            self.progressView.layer.cornerRadius = 10
            
            self.activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            self.activityIndicator.style = .whiteLarge
            self.activityIndicator.center = CGPoint(x: self.progressView.bounds.width / 2, y: self.progressView.bounds.height / 2);
            self.activityIndicator.hidesWhenStopped = true;
            
            self.progressView.addSubview(self.activityIndicator)
            self.containerView.addSubview(self.progressView)
            view.addSubview(self.containerView)
            
            self.activityIndicator.startAnimating()
        }
    }
    
    open func hideProgressView() {
        
        activityIndicator.stopAnimating()
        containerView.removeFromSuperview()
    }
}
