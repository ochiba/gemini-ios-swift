import UIKit

class OneEventTableViewCell: UITableViewCell {
    @IBOutlet weak var labelName: UILabel?
    @IBOutlet weak var imageDate: UIImageView?
    @IBOutlet weak var labelCityAndState: UILabel?
}

class OneMonthTableViewController : GeminiTableViewController {
    
    var arrayEvents   = [Event]();
    var year: Int = 2017;
    var month: Int = 1;
    weak var raceTableViewController: RaceTableViewController?;

    override func viewDidLoad() {
        super.viewDidLoad();
        startDownloading();
    }

    override func startDownloading() {
        super.startDownloading();
        downloadEvents();
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayEvents.count;
    }

    func setYearAndMonth(_ new_Year: Int, newMonth new_Month: Int)
    {
        year = new_Year;
        month = new_Month;
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 60;
    }
    
    // Only upcoming events is editable (can show Sign Up when swiped).
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        let oneEvent = arrayEvents[indexPath.row];
////        return [self determineFuture: oneEvent.post_date];
        return false;
    }
  
    private func downloadEvents() {
        
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=6&year=_\(year)&month=\(month)")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                    self.dataDownloaded();
                }
                return
            }
            
            do {
                let jsonEvents = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                if (jsonEvents.count > 0) {
                    self.arrayEvents.removeAll();
                    
                    for index in 0...(jsonEvents.count - 1) {
                        
                        let jsonElement = jsonEvents[index] as! [String : AnyObject];
                        
                        let primaryKey      = Int(jsonElement["ID"] as! String);
                        let oneEvent        = Event(primaryKey!);
                        oneEvent.post_date  = jsonElement["post_date"] as! String;
                        oneEvent.post_title = jsonElement["post_title"] as! String;
                        oneEvent.city       = (jsonElement["_grr_race_location_city"] as? String)!;
                        oneEvent.state      = (jsonElement["_grr_race_location_state"] as? String)!;
                        
                        self.arrayEvents.append(oneEvent);
                    }
                }
                else {
                    DispatchQueue.main.async {
                        self.showMessage("No Events found.");
                    }
                }
                self.dataDownloaded();
            }
            catch
            {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                    self.dataDownloaded();
                }
            }
        }
        task.resume();
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        
        let oneEvent = arrayEvents[(indexPath?.row)!];
        
        self.raceTableViewController = segue.destination as? RaceTableViewController;
        self.raceTableViewController?.currentEvent(oneEvent);
        self.raceTableViewController?.title = oneEvent.post_title;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell        = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! OneEventTableViewCell;
        let oneEvent    = arrayEvents[indexPath.row];
        
        // If either City or State is blank, then show nothing.
        if (!oneEvent.city.isEmpty && !oneEvent.state.isEmpty) {
            cell.labelCityAndState?.text = oneEvent.city + ", " + oneEvent.state;
        }
        else
        {
            cell.labelCityAndState?.text = "";
        }

        // Extract day only.  Year and Month are shown in the title.
        var day = oneEvent.post_date.components(separatedBy: "-")[2];
        day = day.components(separatedBy: " ")[0];

        cell.imageDate?.image = UIImage(named: "cal\(day).png");

        cell.labelName?.text = oneEvent.post_title;
        
        // Change row color alternately
        if (indexPath.row % 2 != 0) {
            cell.backgroundColor = lightGray
        }
        else {
            cell.backgroundColor = UIColor.white;
        }
        
        return cell;
    }
}

extension String {
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start..<end])
    }
    
    subscript (r: ClosedRange<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound)
        return String(self[start...end])
    }
}
