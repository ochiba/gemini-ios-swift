//
//  QuickResult.swift
//  Gemini
//
//  Created by Osamu Chiba on 6/15/17.
//  Copyright © 2017 Opix. All rights reserved.
//

import UIKit

let TOP3_NOT_RECEIVED   = 0
let TOP3_RECEIVED       = 1
let TOP3_NOT_PRESENT    = 2

class QuickResult: NSObject {
    var ID: Int             = 0;
    var bibNumber:  Int     = 0;
    var overall             = 0;
    var chipTime:   String  = "";
    var tableName: String   = "";
    var firstName: String   = "";
    var lastName:  String   = "";
    var raceTitle: String   = "";
    var display1: String    = "";
    var display2: String    = "";
    var display3: String    = "";
    var display4: String    = "";
    var display5: String    = "";
    var awarded             = TOP3_NOT_RECEIVED;
    var follow_id           = 0;
    
    init(_ pk: Int)
    {
        bibNumber   = pk;
        ID          = pk;
        super.init();
    }
}

class Bookmark: QuickResult
{
    var row_id:  Int?;
    var post_id     = 0;
    var post_title:   String?;
    var post_date: String?;
    
    override init(_ pk: Int)
    {
        row_id = pk;
        super.init(pk);
    }
}

class DetailResult: QuickResult
{
    var age             = 0;
    var divisionPlace   = 0;
    var genderPlace     = 0;
    
    var overallTotal    = 0;
    var divisionTotal   = 0;
    var genderTotal     = 0;

    var city            = "";
    var state           = "";
    var country         = "";

    var gender          = "";
    var division        = "";
    
    var officialTimeField   = "Chip";
    
    override init(_ pk: Int)
    {
        super.init(pk);
    }
    
    func copy(_ source: QuickResult) {
        self.overall    = source.overall;
        self.chipTime   = source.chipTime;
        self.tableName  = source.tableName;
        self.firstName  = source.firstName;
        self.lastName   = source.lastName;
        self.raceTitle  = source.raceTitle;
        self.display1   = source.display1;
        self.display2   = source.display2;
        self.display3   = source.display3;
        self.display4   = source.display4;
        self.display5   = source.display5;
        self.awarded    = source.awarded;
        self.follow_id  = source.follow_id;
    }
}
