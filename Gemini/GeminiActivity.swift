import UIKit

class GeminiActivity : UIActivity {
    var activityImageName   = "";
    var geminiActivityTitle = "";
    var geminiActivityType = "";
    
    init(_ newType : String, title newTitle: String, imageName newImageName: String)
    {
        activityImageName   = newImageName;
        geminiActivityTitle = newTitle;
        geminiActivityType  = newType;
        super.init();
    }

    override var activityImage: UIImage? {
        return UIImage(named: activityImageName)!;
    }

    override var activityTitle: String? {
        return self.geminiActivityTitle;
    }
    
    override var activityType: UIActivity.ActivityType? {
        return UIActivity.ActivityType(rawValue: self.geminiActivityType);
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        return true;
    }
    
    override func prepare(withActivityItems activityItems: [Any])
    {
    }
    
    override func perform() {
        self.activityDidFinish(true);
    }
}
//
//- (UIViewController *)activityViewController
//{
//    NSLog(@"%s",__FUNCTION__);
//    return nil;
//}
