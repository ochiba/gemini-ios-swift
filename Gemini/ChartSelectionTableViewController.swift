import UIKit

class ChartSelectionTableViewController : GeminiTableViewController {
    
    var tableName: String = "";
    weak var distributionChartViewController: DistributionChartViewController?;
    var arrayColumns        = [String]();
    
    override func viewDidLoad() {
        super.viewDidLoad();
        startDownloading();
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayColumns.count;
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false;
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false;
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "distributionCell", for: indexPath)
        cell.textLabel?.text = self.arrayColumns[indexPath.row]; // == 0 ? "By Division" : "By Finish Time";
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;

        self.distributionChartViewController = segue.destination as? DistributionChartViewController;
        self.distributionChartViewController?.tableName(tableName);
        self.distributionChartViewController?.distributionType((indexPath?.row)!);
        self.distributionChartViewController?.selectedColumn = self.arrayColumns[(indexPath?.row)!];
    }

    func tableName(_ newTableName : String)
    {
        tableName = newTableName;
    }
    
    override func startDownloading()
    {
        super.startDownloading();
        
        self.arrayColumns.removeAll();
        self.arrayColumns.append("By Division");
        self.arrayColumns.append("By Finish Time");
        
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=10&table=\(self.tableName)&time_only=1")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
                return
            }
            
            do {
                let array = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [String];
                
                if (array.isEmpty) {
                    return;
                }
                
                for k in 0..<array.count {
                    
                    let oneColumn   = array[k].uppercased();
                    
                    if (oneColumn.caseInsensitiveCompare("GUN") != ComparisonResult.orderedSame &&
                        oneColumn.caseInsensitiveCompare("TOTAL TIME") != ComparisonResult.orderedSame &&
                        oneColumn.caseInsensitiveCompare("CHIP") != ComparisonResult.orderedSame &&
                        oneColumn.caseInsensitiveCompare("T1") != ComparisonResult.orderedSame &&
                        oneColumn.caseInsensitiveCompare("T2") != ComparisonResult.orderedSame &&
                        oneColumn.range(of: "PACE") == nil)
                    {
                        self.arrayColumns.append("By " + array[k]);
                    }
                }
                
                
                self.dataDownloaded();
            } catch {
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }
            }
        }
        task.resume();
    }
}


