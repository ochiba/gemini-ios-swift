import UIKit

class GeminiDatabase: NSObject {
    var database: OpaquePointer?;
    var geminiDatabasePath = "";

    override init()
    {
        super.init();

        if (!self.createGeminiDatabase()) {
            _ = self.createGeminiTables();
        }

        self.addFollowID();
        
        // Quick update.
        self.execute1Query("UPDATE [Events] SET [post_status] = 'publish' WHERE [post_status] <> 'publish' AND [post_date] < date('NOW')");
    }
    
    func createGeminiDatabase() -> Bool
    {
        let filemgr = FileManager.default;
        let sharedCache = URLCache(memoryCapacity:0, diskCapacity: 0, diskPath:nil);
        URLCache.shared = sharedCache;
        
        let dirPaths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true);
        
        let docsDir = dirPaths[0];
        
        /* create path to cache directory inside the application's Documents directory */
        let geminiPath           = docsDir + "/Gemini";
               
        if (!filemgr.fileExists(atPath: geminiPath))
        {
            /* create a new cache directory
            if (!filemgr.createDirectory(atPath: geminiPath, withIntermediateDirectories: false, attributes: nil))
            {
            // [self showErrorMessage: "Failed to create Gemini directory."];
            }*/
            do {
                try(filemgr.createDirectory(atPath: geminiPath, withIntermediateDirectories: false, attributes: nil))
            } catch {
                print("Error -> \(error)")
            }
        }
    
        // Build the path to the database file
        self.geminiDatabasePath = geminiPath + "/Gemini.sqlite";
        
        return filemgr.fileExists(atPath: self.geminiDatabasePath);
    }

    func createGeminiTables() -> Bool
    {
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            var sql_stmt = "CREATE TABLE IF NOT EXISTS [Events] ([ID] INTEGER PRIMARY KEY NOT NULL UNIQUE, [post_date] TEXT default '0000-00-00 00:00:00', [post_title] TEXT NOT NULL, [city] TEXT  default '', [state] TEXT  default 'CA', [post_status] TEXT default 'publish', [post_modified] TEXT default '0000-00-00 00:00:00');";

            if (sqlite3_exec(database, sql_stmt, nil, nil, nil) != SQLITE_OK)
            {
                return false;
            }
            
            // Remove the last 2 fields.
            sql_stmt = "CREATE TABLE IF NOT EXISTS [Bookmarks] ([post_id] INTEGER DEFAULT 0, [race] TEXT DEFAULT '', [table] TEXT DEFAULT '', [Bib Number] INTEGER DEFAULT 0, [Overall] INTEGER DEFAULT 0, [First Name] TEXT DEFAULT '', [Last Name] TEXT DEFAULT '', [Chip Time] TEXT DEFAULT '00:00:00', [Follow ID] INTEGER DEFAULT 0);";
            
            if (sqlite3_exec(database, sql_stmt, nil, nil, nil) != SQLITE_OK)
            {
                return false;
            }

            sqlite3_close(database);
        } else {
            return false;
        }
        return true;
    }

    func execute1Query(_ sql: String) -> Bool
    {
        if (sql.isEmpty) {
            return false;
        }
        
        var bSuccess = false;
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            bSuccess = sqlite3_exec(database, (sql as NSString).utf8String, nil, nil, nil) == SQLITE_OK;
            
            sqlite3_close(database);
        }
        return bSuccess;
    }

    func getOneInteger(_ sql: String) -> Int
    {
        var oneNumber: Int = 0;
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            var selectstmt: OpaquePointer? = nil
            
            if (sqlite3_prepare_v2(database, (sql as NSString).utf8String, -1, &selectstmt, nil) == SQLITE_OK)
            {
                while(sqlite3_step(selectstmt) == SQLITE_ROW)  {
                    oneNumber = Int(sqlite3_column_int(selectstmt, 0));
                }
            }
            sqlite3_finalize(selectstmt);
            sqlite3_close(database);
        }
        return oneNumber;
    }

    func add1Event(_ post_id : Int, newpostTitle new_post_title: String, newPostDate new_post_date: String, newCity new_city: String, newState new_state: String) -> Bool
    {
        var nSuccess = false;
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            let post_title  = new_post_title.replacingOccurrences(of: "'", with: "''");
            let city        = new_city.replacingOccurrences(of: "'", with: "''");

            let sql = "INSERT OR IGNORE INTO [Events] ([ID], [post_title], [post_date], [city], [state]) VALUES('\(post_id)', '\(post_title)', '\(new_post_date)', '\(city)', '\(new_state)');";
        
            nSuccess = self.execute1Query(sql);
            sqlite3_close(database);
        }
        return nSuccess;
    }

    func getArrayEvents(_ sql: String) -> [Event]
    {
        var arrayEvents = [Event]();
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            var selectstmt: OpaquePointer? = nil
        
            if (sqlite3_prepare_v2(database, (sql as NSString).utf8String, -1, &selectstmt, nil) == SQLITE_OK)
            {
                while (sqlite3_step(selectstmt) == SQLITE_ROW)
                {
                    let primaryKey      = sqlite3_column_int(selectstmt, 0);
                    let oneEvent        = Event(Int(primaryKey));
                    oneEvent.post_date  = String(cString: sqlite3_column_text(selectstmt, 1));
                    oneEvent.post_title = String(cString: sqlite3_column_text(selectstmt, 2));
                    oneEvent.city       = String(cString: sqlite3_column_text(selectstmt, 3));
                    oneEvent.state      = String(cString: sqlite3_column_text(selectstmt, 4));
                    
                    arrayEvents.append(oneEvent);
                }
            }
            sqlite3_finalize(selectstmt);
            sqlite3_close(database);
        }
        return arrayEvents;
    }

    func fillBookmarkArray() -> [Bookmark]
    {
        var arrayBookmarks = [Bookmark]();
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            var selectstmt: OpaquePointer? = nil

            if (sqlite3_prepare_v2(database, "SELECT [Bookmarks].[ROWID], [Bookmarks].[post_id], [Bookmarks].[race], [Bookmarks].[table], [Bookmarks].[Bib Number], [Bookmarks].[Overall], [Bookmarks].[First Name], [Bookmarks].[Last Name], [Bookmarks].[Chip Time], [Events].[post_title], [Events].[post_date], [Bookmarks].[Follow ID] FROM [Bookmarks] LEFT JOIN [Events] ON [Bookmarks].[post_id] = [Events].[ID] ORDER BY [Bookmarks].[ROWID] DESC", -1, &selectstmt, nil) == SQLITE_OK)
            {
                while (sqlite3_step(selectstmt) == SQLITE_ROW)
                {
                    let primaryKey          = sqlite3_column_int(selectstmt, 0);
                    let oneBookmark         = Bookmark(Int(primaryKey));
                    oneBookmark.post_id     = Int(sqlite3_column_int(selectstmt, 1));
                    
                    oneBookmark.raceTitle   = String(cString: sqlite3_column_text(selectstmt, 2));
                    oneBookmark.tableName   = String(cString: sqlite3_column_text(selectstmt, 3));
                    oneBookmark.bibNumber   = Int(sqlite3_column_int(selectstmt, 4));
                    oneBookmark.overall     = Int(sqlite3_column_int(selectstmt, 5));
                    
                    oneBookmark.firstName   = String(cString: sqlite3_column_text(selectstmt, 6));
                    oneBookmark.lastName    = String(cString: sqlite3_column_text(selectstmt, 7));
                    oneBookmark.chipTime    = String(cString: sqlite3_column_text(selectstmt, 8));
                    oneBookmark.post_title  = String(cString: sqlite3_column_text(selectstmt, 9));
                    oneBookmark.post_date   = String(cString: sqlite3_column_text(selectstmt, 10));
                    oneBookmark.follow_id   = Int(sqlite3_column_int(selectstmt, 11));
                    
                    arrayBookmarks.append(oneBookmark);
                }
            }
            sqlite3_finalize(selectstmt);
            sqlite3_close(database);
        }
        return arrayBookmarks;
    }

    // FAvorites and Cart share the same table.  An Integer field [Displayed In] determines which type.
    // http://stackoverflow.com/questions/418898/sqlite-upsert-not-insert-or-replace

    func addResultToBookmarks(_ post_id: Int, newRaceTitle new_race_title: String, newTableName new_table_name: String, newBibNumber new_bib_number: Int,  newOverall new_overall: Int, newFirstName new_first_name: String, newLastName new_last_name: String, newChipTime new_chip_time: String, followID follow_id: Int) -> Bool
    {
        var nSuccess = false;
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            // Can't imagine a first name with a ', but just in case...
            let first_name  = new_first_name.replacingOccurrences(of: "'", with: "''");
            let last_name   = new_last_name.replacingOccurrences(of: "'", with: "''");
            let race_title  = new_race_title.replacingOccurrences(of: "'", with: "''");

            let sql   = "INSERT INTO [Bookmarks] ([post_id], [race], [table], [Bib Number], [Overall], [First Name], [Last Name], [Chip Time], [Follow ID]) VALUES('\(post_id)', '\(race_title)', '\(new_table_name)', '\(new_bib_number)','\(new_overall)', '\(first_name)', '\(last_name)', '\(new_chip_time)', '\(follow_id)');";

            nSuccess = self.execute1Query(sql)
            
            sqlite3_close(database);
        }
        return nSuccess;
    }

    func removeFromBookmarks(_ bookmarkID : Int) -> Bool
    {
        var nSuccess = false;
        
        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            nSuccess = self.execute1Query("DELETE FROM [Bookmarks] WHERE [ROWID] = '\(bookmarkID)';");
            sqlite3_close(database);
        }
        return nSuccess;
    }

    // Dropping a column is not possible by design.
    func addFollowID()
    {
        var bFollowIDFound = false;

        if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
        {
            // get current database version of schema
            var sql: OpaquePointer? = nil
            
            if (sqlite3_prepare_v2(database, "PRAGMA table_info(Bookmarks)", -1, &sql, nil) == SQLITE_OK) {
                while(sqlite3_step(sql) == SQLITE_ROW) {
                    
                    let columnName = String(cString: sqlite3_column_text(sql, 1));

                    if (columnName == "Follow ID") {
                        bFollowIDFound = true;
                        break;
                    }
                }
                
                if (!bFollowIDFound) {
                    bFollowIDFound = self.execute1Query("ALTER TABLE [Bookmarks] ADD COLUMN [Follow ID] INTEGER DEFAULT 0");
                }
            }
            
            sqlite3_finalize(sql);
            sqlite3_close(database);
        }
    }

//- (int) userVersion
//{
//    int databaseVersion = 0;
//    
//    if (sqlite3_open(self.geminiDatabasePath, &database) == SQLITE_OK)
//    {
//        // get current database version of schema
//        static sqlite3_stmt *sql;
//
//        //SELECT name FROM sqlite_master WHERE type='table' AND name='Bookmarks';
//        if (sqlite3_prepare_v2(database, "PRAGMA user_version", -1, &sql, nil) == SQLITE_OK) {
//            while(sqlite3_step(sql) == SQLITE_ROW) {
//                
//                // NSString* table = String(cString: sqlite3_column_text(stmt_version, 0)];
//                databaseVersion = sqlite3_column_int(sql, 0);
//                NSLog("%s: version %d", __FUNCTION__, databaseVersion);
//            }
//            NSLog("%s: the databaseVersion is: %d", __FUNCTION__, databaseVersion);
//        }
//        else {
//            NSLog("%s: ERROR Preparing: , %s", __FUNCTION__, sqlite3_errmsg(database) );
//        }
//        
//        char *errMsg;
//        const char *sql_stmt = "DROP TABLE [Settings];";
//        
//        if (sqlite3_exec(database, sql_stmt, nil, nil, &errMsg) != SQLITE_OK)
//        {
//            [self showErrorMessage: "Failed to create [Bookmarks]."];
//            return false;
//        }
//
//        sqlite3_finalize(sql);
//        sqlite3_close(database);
//    }
//    return databaseVersion;
//}
//
//- (void) setUserVersion: (uint32_t) newVersion
//{
//    if ([self execute1Query: "PRAGMA user_version = \(newVersion)"])
//        NSLog("Succesfully updated to %i", newVersion);
//    else
//        NSLog("Failed to update");
//}
}
