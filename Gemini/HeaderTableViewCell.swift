import UIKit

let DISPLAY_1       = 0
let DISPLAY_2       = 1
let DISPLAY_3       = 2
let DISPLAY_4       = 3
let DISPLAY_5       = 4
let DISPLAY_OVERALL = 5

class HeaderTableViewCell: UITableViewCell {
    @IBOutlet weak var labelResults: UILabel?
    @IBOutlet weak var labelDisplay1: HeaderButton?
    @IBOutlet weak var labelDisplay2: HeaderButton?
    @IBOutlet weak var labelDisplay3: HeaderButton?
    @IBOutlet weak var labelDisplay4: HeaderButton?
    @IBOutlet weak var labelDisplay5: HeaderButton?
    @IBOutlet weak var labelOverall: HeaderButton?
    
    var arrayDisplayColumns = [HeaderButton]();
    var iPad = false;
    var currentSelection = DISPLAY_1;
    
    public required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }

    init(_ isPad: Bool) {
        super.init(style: UITableViewCell.CellStyle.default, reuseIdentifier: "headerCell");
        self.initializeWithiPad(isPad);
    }
    
    public func initializeWithiPad(_ isPad: Bool) {
        iPad = isPad;
        
        // Create column header objects
        labelDisplay1?.name = "display1";
        labelDisplay2?.name = "display2";
        labelDisplay3?.name = "display3";
        
        if (iPad)
        {
            labelDisplay4?.name = "display4";
            labelDisplay5?.name = "display5";
        }
        
        labelOverall?.name = "overall";
        
        self.arrayDisplayColumns.append(labelDisplay1!);
        self.arrayDisplayColumns.append(labelDisplay2!);
        self.arrayDisplayColumns.append(labelDisplay3!);
        
        if (iPad)
        {
            self.arrayDisplayColumns.append(labelDisplay4!);
            self.arrayDisplayColumns.append(labelDisplay5!);
        }
        
        self.arrayDisplayColumns.append(labelOverall!);
        
        let STOP = iPad ? DISPLAY_OVERALL : DISPLAY_4;
        
        for i in DISPLAY_2..<STOP {
            self.setTitleAtIndex("", index: i);
        }    
    }
    
    func setTitleAtIndex(_ newTitle: String, index location: Int)
    {
        let button = self.arrayDisplayColumns[location];
        button.setTitle(newTitle, for: UIControl.State.normal);
    }
    
    func titleAtIndex(_ index: Int) -> String
    {
        let button = self.arrayDisplayColumns[index];
        return button.title(for: UIControl.State.normal)!
    }

    // Returns newly selected button.
    func setNewCurrentSelection(_ newSelection: Int) -> HeaderButton
    {
        currentSelection = newSelection;
        let STOP = iPad ? DISPLAY_OVERALL : DISPLAY_4;
    
        for i in 0...STOP
        {
            let button = self.arrayDisplayColumns[i];
        
            if (currentSelection == i) {
                button.ascending = !button.ascending;
            }
            else
            {
                button.ascending = false;
            }
        
            DispatchQueue.main.async {
                if (button.title(for: UIControl.State.normal) != "")
                {
                    button.layer.borderWidth = self.currentSelection == i ? 2.0 : 1.0;
                    button.layer.borderColor = UIColor.white.cgColor;
                    button.layer.cornerRadius = 5.0;
                }
                else {
                    button.layer.borderColor = UIColor.clear.cgColor;
                }
            }
        }
        return arrayDisplayColumns[currentSelection];
    }
}

class ThreeColumnTableViewCell: UITableViewCell {
    @IBOutlet weak var labelDisplay1: UILabel?
    @IBOutlet weak var labelDisplay2: UILabel?
    @IBOutlet weak var labelDisplay3: UILabel?
}

class OneResultTableViewCell: ThreeColumnTableViewCell {
    @IBOutlet weak var labelName: UILabel?;
    @IBOutlet weak var labelDisplay4: UILabel?;
    @IBOutlet weak var labelDisplay5: UILabel?;
    @IBOutlet weak var labelPlace: UILabel?;
    @IBOutlet weak var labelCounter: UILabel?;
}

class EventTableViewCell : ThreeColumnTableViewCell {
    @IBOutlet weak var logo: UIImageView?;
}

class BookmarkTableViewCell : UITableViewCell {

    @IBOutlet weak var labelEvent: UILabel?;
    @IBOutlet weak var labelDate: UILabel?;
    @IBOutlet weak var labelAthleteName: UILabel?;
    @IBOutlet weak var labelOverall: UILabel?;
    @IBOutlet weak var labelRaceAndTime: UILabel?;
}

class OfficialTimeTableViewCell : UITableViewCell {
    @IBOutlet weak var labelOfficialTime: UILabel?;
}
