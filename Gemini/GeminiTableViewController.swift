import UIKit
import UserNotifications

let lightGray = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.1);

let TAB_EVENTS      = 0
let TAB_BOOKMARKS   = 1
let TAB_MORE        = 2

let COLUMN_CHIP     = "Chip";
let COLUMN_GUN      = "Gun";
let COLUMN_TOTAL_TIME = "Total Time";

class GeminiTableViewController: UITableViewController {

    var isFuture        = false;
    var currentEvent: Event?;
    var iPad            = false;
    var messageController: UIAlertController?;
    var addingFollow = false;

    var config :URLSessionConfiguration! = URLSessionConfiguration.default;
    var session:URLSession!;
    let appDelegate = UIApplication.shared.delegate as! AppDelegate;

    let indicator = ProgressView();

    override func viewDidLoad() {
        super.viewDidLoad();
        session = URLSession(configuration: config);

        // Change the color of < (back button):
        self.navigationController?.navigationBar.tintColor = UIColor.white;
        // Swiping down the table view triggers downloading events.
        // http://www.appcoda.com/pull-to-refresh-uitableview-empty/
        self.refreshControl = UIRefreshControl();
        self.refreshControl?.backgroundColor = geminiRed;
        self.refreshControl?.tintColor = UIColor.white;
        self.refreshControl?.addTarget(self, action: #selector(startDownloading), for: .valueChanged)
        
        let deviceType = UIDevice.current.model;
        iPad = deviceType == "iPad";
    }

    @objc func startDownloading()
    {
        DispatchQueue.main.async {
            self.indicator.showProgressView(self.tableView);
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0;
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return false;
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return false;
    }
    
    // No need to do anything but still needed.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    func future(_ newFuture: Bool)
    {
        isFuture = newFuture;
    }
    
    func future() -> Bool {
        return isFuture;
    }
    
    // http://stackoverflow.com/questions/3312935/nsnumberformatter-and-th-st-nd-rd-ordinal-number-endings
    func ordinalNumberFormat(_ number: Int) -> String
    {
        if (11...13).contains(number % 100) {
            return "\(number)th"
        }
        
        switch number % 10 {
            case 1:
                return "\(number)st"
        
            case 2:
                return "\(number)nd"
        
            case 3:
                return "\(number)rd"
            
            default:
                return "\(number)th"
        }
    }
    
    func currentEvent(_ newEvent :Event)
    {
        self.currentEvent = newEvent;
    }

    @objc func clearBookmarksBadge()
    {
        self.tabBarController?.tabBar.items?[1].badgeValue = nil;
    }
    
    func formatDate(_ dateString : String) -> String
    {
        let formatter = DateFormatter();
  
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
        let date = formatter.date(from: dateString);
        
        formatter.timeStyle = .none;
        formatter.dateStyle = .long;
        formatter.locale    = Locale(identifier: "en_US");

        return formatter.string(from: date!);
    }

    func dataDownloaded()
    {
        DispatchQueue.main.async {
            self.tableView.reloadData();
            self.indicator.hideProgressView();
            self.refreshControl?.endRefreshing();
        }
    }
    
    func showMessage(_ newMesage: String)
    {
        messageController = UIAlertController(title: "Gemini Live", message: newMesage, preferredStyle: .alert);
        self.present(messageController!, animated: true);
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(hideMessage), userInfo: nil, repeats: false)
    }
    @objc  
    func hideMessage()
    {
        messageController?.dismiss(animated: true);
    }

    func generateDetailResult(column originalColumnArray: [String],  data originalDataArray: [String], quickResult oneResult: QuickResult) -> (columnArray: [String], dataArray : [String], detailResult: DetailResult)
    {
        let detailResult = DetailResult(oneResult.ID);
        detailResult.copy(oneResult);
        
        var columnArray = originalColumnArray;
        var dataArray   = originalDataArray;
        
        if (columnArray.count != dataArray.count || dataArray.isEmpty || columnArray.isEmpty)
        {
            return (columnArray, dataArray, detailResult);
        }
        
        var chipIndex           = -1;
        var gunIndex            = -1;
        var totalIndex          = -1;

        // Start the loop from the end so that deleting records will not change the index.
        for j in stride(from: (columnArray.count - 1), through: 0, by: -1) {
            let column = columnArray[j];
            
            if (column.caseInsensitiveCompare("FIRST NAME") == ComparisonResult.orderedSame)
            {
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("LAST NAME") == ComparisonResult.orderedSame)
            {
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }

            if (column.caseInsensitiveCompare("BIB") == ComparisonResult.orderedSame)
            {
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }

            if (column.caseInsensitiveCompare("AGE") == ComparisonResult.orderedSame)
            {
                detailResult.age = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("SEX") == ComparisonResult.orderedSame)
            {
                detailResult.gender = dataArray[j].replacingOccurrences(of: "\\'", with: "'");
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("CITY") == ComparisonResult.orderedSame)
            {
                detailResult.city = dataArray[j].replacingOccurrences(of: "\\'", with: "'");
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("STATE") == ComparisonResult.orderedSame)
            {
                detailResult.state = dataArray[j].replacingOccurrences(of: "\\'", with: "'");
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("COUNTRY") == ComparisonResult.orderedSame)
            {
                detailResult.country = dataArray[j].replacingOccurrences(of: "\\'", with: "'");
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("DIVISION") == ComparisonResult.orderedSame)
            {
                detailResult.division = dataArray[j].replacingOccurrences(of: "\\'", with: "'");
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("DIVISION PLACE") == ComparisonResult.orderedSame)
            {
                detailResult.divisionPlace = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("DIVISION TOTAL") == ComparisonResult.orderedSame)
            {
                detailResult.divisionTotal = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("TOTAL RUNNERS") == ComparisonResult.orderedSame || column.caseInsensitiveCompare("TOTAL COMPETITORS") == ComparisonResult.orderedSame)
            {
                detailResult.overallTotal = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("OVERALL") == ComparisonResult.orderedSame)
            {
                detailResult.overall = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("GENDER TOTAL") == ComparisonResult.orderedSame)
            {
                detailResult.genderTotal = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
            
            if (column.caseInsensitiveCompare("SEX PLACE") == ComparisonResult.orderedSame)
            {
                detailResult.genderPlace = Int(dataArray[j])!;
                
                dataArray.remove(at: j);
                columnArray.remove(at: j);
            }
        }
        
        // Determine what is used as "Official Time"
        for k in stride(from: (columnArray.count - 1), through: 0, by: -1) {
            let column = columnArray[k];

            if (column.caseInsensitiveCompare(COLUMN_CHIP) == ComparisonResult.orderedSame)
            {
                chipIndex = k;
                break;
            }
            
            if (column.caseInsensitiveCompare(COLUMN_TOTAL_TIME) == ComparisonResult.orderedSame)
            {
                totalIndex = k;
            }
            
            if (column.caseInsensitiveCompare(COLUMN_GUN) == ComparisonResult.orderedSame)
            {
                gunIndex = k;
            }
        }
        
        if (totalIndex != -1) {
            detailResult.chipTime = dataArray[totalIndex].replacingOccurrences(of: "\\'", with: "'");
            dataArray.remove(at: totalIndex);
            columnArray.remove(at: totalIndex);
        }
        else if (chipIndex != -1) {
            detailResult.chipTime = dataArray[chipIndex].replacingOccurrences(of: "\\'", with: "'");
            dataArray.remove(at: chipIndex);
            columnArray.remove(at: chipIndex);
        }
        else if (gunIndex != -1) {
            detailResult.chipTime = dataArray[gunIndex].replacingOccurrences(of: "\\'", with: "'");
            dataArray.remove(at: gunIndex);
            columnArray.remove(at: gunIndex);
        }

        return (columnArray, dataArray, detailResult);
    }

    func generateSocialMediaBody(_ columnArray: [String],  data dataArray: [String], detailResult oneResult: DetailResult) -> String
    {
        var body   = "Event: \(currentEvent!.post_title)\nDate: ";
        
        body += self.formatDate(currentEvent!.post_date);
        body += "\nLocation: \(currentEvent!.city), \(currentEvent!.state)";
        body += "\nRace: \(oneResult.raceTitle)\n";

        body += "\nAthlete: \(oneResult.firstName) \(oneResult.lastName)";
        body += "\nAge: \(oneResult.age)";
        body += "\nGender: \(oneResult.gender)";
        body += "\nHometown: \(oneResult.city), \(oneResult.state) \(oneResult.country)";
        body += "\nBib Number: #\(oneResult.bibNumber)";
        body += "\nDivision: \(oneResult.division)\n";

        body += "\nOfficial Time: \(oneResult.chipTime)\n";

        body += "\nOverall: " + formatRanking(oneResult.overall, oneResult.overallTotal);
        body += "\nDivision Place: " + formatRanking(oneResult.divisionPlace, oneResult.divisionTotal);
        body += "\nGender Place: " + formatRanking(oneResult.genderPlace, oneResult.genderTotal) + "\n";

        for i in 0..<columnArray.count
        {
            body += "\n\(columnArray[i]): \(dataArray[i])";
        }
        return body;
    }

    func formatRanking(_ place: Int, _ totalCount: Int) -> String
    {
        if (place == 0 && totalCount == 0) {
            return "N/A";
        }
        else if (place != 0 && totalCount == 0){
            return ordinalNumberFormat(place);
        }

        return ordinalNumberFormat(place) + " / \(totalCount)";
    }
    
    func add1Follower(_ quickResult: QuickResult)
    {
        let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound];
        
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { granted,_ in
            
            if (granted) {
                self.appDelegate.firMessageDelegate();

                self.addingFollow = true;
                
                let callURL = URL.init(string: "https://gemininext.com/mobile/?action=11&table=\(quickResult.tableName)&bib_number=\(quickResult.bibNumber)&token=\(self.appDelegate.deviceToken())")
                var request = URLRequest.init(url: callURL!)
                
                request.timeoutInterval = 30.0;
                request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
                request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
                request.httpMethod = HTTPMethod_Post
                
                let task = self.session.dataTask(with: request) { (data, response, error) -> Void in
                    
                    if error != nil{
                        DispatchQueue.main.async {
                            self.showMessage("Network Error");
                        }

                        return
                    }

                    do {
                        let jsonElement = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! NSDictionary;
                        
                        if (jsonElement.count > 0)
                        {
                            let newFollowID = jsonElement["follow_id"] as! Int;
                            
                            if (newFollowID > 0)
                            {
                                quickResult.follow_id = newFollowID;
                                
                                DispatchQueue.main.async {
                                    self.add1Favorite(quickResult);
                                }
                            }
                        }
                        
                    } catch {
                        DispatchQueue.main.async {
                            self.showMessage("Network Error");
                        }
                    }
                }
                
                task.resume();
            }
            else {
                DispatchQueue.main.async {
                    self.showMessage("Notification is not allowed.  Please change in Settings.");
                }
            }
        });
    }
    
    func delete1Follower(_ follow_id: Int)
    {
        let callURL = URL.init(string: "https://gemininext.com/mobile/?action=12&follow_id=\(follow_id)")
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            if error != nil{
                DispatchQueue.main.async {
                    self.showMessage("Network Error");
                }

                return
            }
        }
        
        task.resume();
    }

    func isFuture(_ date_string : String) -> Bool
    {
        let formatter           = DateFormatter();
        formatter.locale        = Locale(identifier: "en_US");
        formatter.dateFormat    = "yyyy-MM-dd HH:mm:ss";
        let post_date           = formatter.date(from: date_string);
        let now                 = Date();
        
        return post_date?.compare(now) == ComparisonResult.orderedDescending;
    }
    
    func add1Favorite(_ quickResult: QuickResult) -> Bool {
        let geminiDB = GeminiDatabase();
        
        if (geminiDB.add1Event((currentEvent?.ID)!, newpostTitle:(currentEvent?.post_title)!, newPostDate:(currentEvent?.post_date)!, newCity: (currentEvent?.city)!, newState: (currentEvent?.state)!))
        {
            if (geminiDB.addResultToBookmarks((currentEvent?.ID)!, newRaceTitle: quickResult.raceTitle, newTableName: quickResult.tableName, newBibNumber: quickResult.bibNumber, newOverall : quickResult.overall, newFirstName: quickResult.firstName, newLastName: quickResult.lastName, newChipTime: quickResult.chipTime, followID: quickResult.follow_id))
            {
                // Show how many successfully added to the Favorites page in the tab badge.
                self.tabBarController?.tabBar.items?[1].badgeValue = "+1";

                // 1.5 seconds later, clear the badge.
                Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(clearBookmarksBadge), userInfo: nil, repeats: false);
                return true;
            }
        }
        return false;
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
    {
        super.viewWillTransition(to: size, with: coordinator);
    }
}
