import UIKit;
import Charts

let TYPE_DIVISION   = 0
let TYPE_FINISHERS  = 1

class DistributionChartViewController: UIViewController, IAxisValueFormatter {

    var tableName = "";
    var distributionType = TYPE_DIVISION;
    
    var currentKeyword = "";
    var bDivision = true;
    var display1        = "";
    var display2        = "";
    var display3        = "";
    var selectedColumn  = "";
    
    var top3 = false;
    var xParticipantGroup = [String]();
    @IBOutlet var chartView: HorizontalBarChartView?;

    let indicator = ProgressView();

    override func viewDidLoad()
    {
        super.viewDidLoad();

        self.chartView?.chartDescription?.text = "";
        
        self.chartView?.drawBarShadowEnabled = false;
        self.chartView?.drawValueAboveBarEnabled = true;

        self.chartView?.maxVisibleCount = 300;
        self.chartView?.pinchZoomEnabled = false;
        self.chartView?.drawGridBackgroundEnabled = false;
        self.chartView?.extraTopOffset = 10;
        
        self.chartView?.noDataText = "";
        
        let xAxis = self.chartView?.xAxis;
        xAxis?.labelPosition = XAxis.LabelPosition.bottom;
        xAxis?.labelFont = UIFont.systemFont(ofSize: 10);
        xAxis?.drawAxisLineEnabled = true;
        xAxis?.drawGridLinesEnabled = false;
        xAxis?.valueFormatter = self;// as? IAxisValueFormatter;
        xAxis?.xOffset = 15;

        let rightAxis = self.chartView?.rightAxis;
        rightAxis?.labelFont = UIFont.systemFont(ofSize: 10);
        rightAxis?.drawAxisLineEnabled = true;
        rightAxis?.drawGridLinesEnabled = false;
        rightAxis?.axisMinimum = 0;

        let leftAxis = chartView?.leftAxis;
        leftAxis?.labelFont = UIFont.systemFont(ofSize: 10);
        leftAxis?.drawAxisLineEnabled = true;
        leftAxis?.drawGridLinesEnabled = true;
        leftAxis?.axisMinimum = 0;

        self.chartView?.legend.form = Legend.Form.square;
        self.chartView?.legend.formSize = 8.0;
        self.chartView?.legend.font = UIFont(name:"HelveticaNeue-Light", size:11)!;
        self.chartView?.legend.xEntrySpace = 4.0;
        
        self.indicator.showProgressView(self.view);
        self.title = selectedColumn;
        
        if (TYPE_FINISHERS <= distributionType)
        {
            var url = "https://gemininext.com/mobile/?action=16&table=\(tableName)";
            
            if (TYPE_FINISHERS < distributionType) {
                // 1) Remove "By ".
                // 2) Encode space with %20.
                let escapedString = selectedColumn.replacingOccurrences(of: "By ", with: "").addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed);
                url += "&column=" + escapedString!;
            }
            else {
                selectedColumn = "";
            }
            
            downloadDistribution(url);
        }
        else if (TYPE_DIVISION == distributionType)
        {
            selectedColumn = "";
            downloadDistribution("https://gemininext.com/mobile/?action=15&table=\(tableName)");
        }
    }

    func downloadDistribution(_ url: String) {

        let callURL = URL.init(string: url)
        var request = URLRequest.init(url: callURL!)
        
        request.timeoutInterval = 30.0;
        request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringLocalCacheData
        request.addValue(ContentType_ApplicationJson, forHTTPHeaderField: HTTPHeaderField_ContentType)
        request.httpMethod = HTTPMethod_Post
        let config :URLSessionConfiguration! = URLSessionConfiguration.default;
        let session = URLSession(configuration: config);
        
        let task = session.dataTask(with: request) { (data, response, error) -> Void in
            
            if error != nil {
                return
            }
            
            do {
                let jsonArray = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments) as! [AnyObject];
                
                if (jsonArray.count <= 0) {
                    DispatchQueue.main.async {
                        self.indicator.hideProgressView();
                    }
                    return;
                }
                
                self.xParticipantGroup.removeAll();
                var yVals = [BarChartDataEntry]();
                
                for i in 0..<jsonArray.count {
                    
                    let jsonElement = jsonArray[i] as! [String];

                    if (TYPE_DIVISION != self.distributionType)
                    {
                        self.xParticipantGroup.append(self.formatXValue(jsonElement[0], !(self.selectedColumn.isEmpty)));
                    }
                    else
                    {
                        self.xParticipantGroup.append(jsonElement[0]);
                    }
                    let yValue = Double(jsonElement[1]);
                    
                    yVals.append(BarChartDataEntry(x: Double(i), y: yValue!));
                }
                
                let set1 = BarChartDataSet(values: yVals, label: "Number of Participants");
                
                set1.valueFormatter = ParticipantsValueFormatter();
                
                let xAxis = self.chartView?.xAxis;
                xAxis?.labelCount = jsonArray.count;
                
                let data = BarChartData(dataSet: set1);
                data.setValueFont(UIFont(name: "HelveticaNeue-Light", size:10));
                
                self.chartView?.data = data;

                DispatchQueue.main.async {
                    self.indicator.hideProgressView();
                }

                self.chartView?.animate(yAxisDuration: 2.5);
            } catch {

                DispatchQueue.main.async {
                    self.indicator.hideProgressView();
                }
            }
        }
        task.resume();
    }
    
    func tableName(_ newTableName: String)
    {
        tableName = newTableName;
    }

    func distributionType(_ newType: Int)
    {
        distributionType = newType;
    }

    func optionTapped(key: NSString)
    {
        if (key == "toggleValues")
        {
            for set in (chartView?.data?.dataSets)!
            {
                set.drawValuesEnabled = !set.isDrawValuesEnabled;
            }
            
            chartView?.setNeedsDisplay();
        }
        
        if (key == "toggleHighlight")
        {
            chartView?.data?.highlightEnabled = !(chartView?.data?.isHighlightEnabled)!;
            chartView?.setNeedsDisplay();
        }
        
        if (key == "toggleStartZero")
        {
//            chartView?.leftAxis.axisMinimum = !(chartView?.leftAxis.axisMinimum);
//            chartView?.rightAxis.axisMinimum = !((chartView?.rightAxis.axisMinimum) != nil);
            
            chartView?.notifyDataSetChanged();
        }
        
        if (key == "animateX")
        {
            chartView?.animate(xAxisDuration: 3.0);
        }
        
        if (key == "animateY")
        {
            chartView?.animate(yAxisDuration: 3.0);
        }
        
        if (key == "animateXY")
        {
            chartView?.animate(xAxisDuration: 3.0, yAxisDuration:3.0);
        }
        
        if (key == "togglePinchZoom")
        {
            chartView?.pinchZoomEnabled = !(chartView?.isPinchZoomEnabled)!;
            
            chartView?.setNeedsDisplay();
        }
        
        if (key == "toggleAutoScaleMinMax")
        {
            chartView?.autoScaleMinMaxEnabled = !(chartView?.isAutoScaleMinMaxEnabled)!;
            chartView?.notifyDataSetChanged();
        }
    }

    // Format x-axis when it's finish time, meaning under hh:mm
    func formatXValue(_ xValue: String, _ fiveMInutes: Bool) -> String //HH:mm:ss
    {
        let increment = fiveMInutes ? 5 : 10;
        let bumpTo1hour = fiveMInutes ? 55 : 50;

        
        let stringArray = xValue.components(separatedBy: ":");
        
        var hours = Int(stringArray[0])!;
        var minutes = Int(stringArray[1])!;
        
        if (minutes >= bumpTo1hour)
        {
            hours += 1;
            minutes = 0;
        }
        else
        {
            minutes += increment;
            minutes = (minutes / increment) * increment;
        }
        return String(format: "%ldh%02ldm >", hours, minutes);
    }

    // Convert time to seconds.
    func convertToSeconds(_ xValue: String) -> Int //HH:mm:ss
    {
        let stringArray = xValue.components(separatedBy: ":");
        
        let hours = Int(stringArray[0])!;
        let minutes = Int(stringArray[1])!;
        let seconds = Int(stringArray[2])!;
        
        return hours * 3600 + minutes * 60 + seconds;
    }


//    - (void) setChart: (NSString*) newKeyword division: (BOOL) newDivision top3: (BOOL) newTop3 display1: (NSString*) newDisplay1  display2: (NSString*) newDisplay2  display3: (NSString*) newDisplay3
//    {
//    top3            = newTop3;
//    bDivision       = newDivision;
//    currentKeyword  = newKeyword;
//    display1        = newDisplay1;
//    display2        = newDisplay2;
//    display3        = newDisplay3;
//    }

    func stringForValue(_ newValue: Double, axis newAxis: AxisBase?) -> String
    {
        if (newAxis is XAxis)
        {
            if (!xParticipantGroup.isEmpty) {
                    return xParticipantGroup[Int(newValue)];
            }
        }
        
        return "";
    }
}
