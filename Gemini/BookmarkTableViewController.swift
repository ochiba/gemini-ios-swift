import UIKit

class BookmarkTableViewController : GeminiTableViewController {

    weak var oneResultTableViewController: OneResultTableViewController?;
    var arrayEvents   = [Event]();
    var arrayBookmarks   = [Bookmark]();

    override func startDownloading()
    {
        // This view does not need to get anything from the website.  So when the view is swiped down, stop the progress circle right away.
        self.refreshControl?.endRefreshing();
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated);
        
        let db = GeminiDatabase();

        arrayBookmarks  = db.fillBookmarkArray();
        arrayEvents     = db.getArrayEvents("SELECT * FROM [Events];");
       
        self.tableView.reloadData();
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayBookmarks.count;
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "bookmarkCell", for: indexPath) as! BookmarkTableViewCell
        
        if (arrayBookmarks.count > 0)
        {
            let oneBookmark  = arrayBookmarks[indexPath.row];
            
            cell.labelEvent?.text        = oneBookmark.post_title;
            cell.labelDate?.text         = self.formatDate(oneBookmark.post_date!);
            
            cell.labelAthleteName?.text  = "\(oneBookmark.firstName) \(oneBookmark.lastName)";
            cell.labelRaceAndTime?.text  = oneBookmark.raceTitle;
            cell.labelOverall?.text      = "\(self.ordinalNumberFormat(oneBookmark.overall)) Overall";
           
            // Change row color alternately
            if (indexPath.row % 2 != 0) {
                cell.backgroundColor = lightGray
            }
            else {
                cell.backgroundColor = UIColor.white;
            }
        }
        return cell;
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }

    override func tableView(_ tableView: UITableView,
                            editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]?
    {
        let removeAction = UITableViewRowAction(style: UITableViewRowAction.Style.default, title: "Remove", handler: {(action, indexPath) -> Void in
        
            let oneBookmark = self.arrayBookmarks[indexPath.row];
            self.removeFromBookmarks(oneBookmark, index: indexPath.row);
            
            self.tableView.reloadData();
        });
        
        removeAction.backgroundColor = geminiRed;
        
        return [removeAction];
    }

    override func dataDownloaded()
    {
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = self.tableView.indexPathForSelectedRow;
        
        let oneBookmark = self.arrayBookmarks[(indexPath == nil) ? 0 : (indexPath?.row)!];
        
        var oneEvent: Event?;
        
        for i in 0..<arrayEvents.count
        {
            oneEvent = arrayEvents[i];
            
            if (oneEvent?.ID == oneBookmark.post_id)
            {
                currentEvent = oneEvent;
                break;
            }
        }

        
        self.oneResultTableViewController = segue.destination as? OneResultTableViewController;
        self.oneResultTableViewController?.quickResult(oneBookmark);
        self.oneResultTableViewController?.currentEvent(currentEvent!);
        self.oneResultTableViewController?.title = "\(oneBookmark.firstName) \(oneBookmark.lastName)";
        self.oneResultTableViewController?.future(oneBookmark.follow_id != 0);
//        self.oneResultTableViewController?.officialTimeField(self.officialTimeField);
    }

    // http://stackoverflow.com/questions/25298196/uialertview-button-action-not-working
    @IBAction func removeAll(_ sender: Any?)
    {
        // Nothing?  Just ignore.
        if (arrayBookmarks.isEmpty) {
            return;
        }

        let alert = UIAlertController(title:"Warning", message: "Delete all?  This can not be un-done.", preferredStyle: UIAlertController.Style.alert);
        
        let defaultAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            alert.dismiss(animated: true);
            
            for i in stride(from: (self.arrayBookmarks.count - 1), through: 0, by: -1) {
                let oneBookmark = self.arrayBookmarks[i];
                self.removeFromBookmarks(oneBookmark, index: i);
            }
            
            self.tableView.reloadData();
        });

        let cancel = UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { (action) -> Void in
            alert.dismiss(animated: true);
        });

        alert.addAction(defaultAction);
        alert.addAction(cancel);
        
        self.present(alert, animated: true, completion: nil);
    }

    func removeFromBookmarks(_ oneBookmark: Bookmark, index deleteIndex: Int)
    {
        // 1) Delete from Database
        _ = GeminiDatabase().removeFromBookmarks(oneBookmark.row_id!);
        
        // 2) Delete from array
        arrayBookmarks.remove(at: deleteIndex);
        
        // 3) If this a follow record, cancel following.
        if (oneBookmark.follow_id != 0) {     
            self.delete1Follower(oneBookmark.follow_id);
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100;
    }
}
